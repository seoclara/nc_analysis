{
	///// Input tree variables ////////////////////////////////////////////
	TChain *itree = new TChain("tree");
	for(int ifile = 0; ifile<10; ifile++){
		//itree->Add(Form("/media/jeewon/Samsung_T5/Sr84/groundtest/data/20211203/ntp/ntp_test.root.%05d",ifile));
		itree->Add(Form("/Volumes/Samsung_T5/Sr84/groundtest/data/20211203/ntp/ntp_test.root.%05d",ifile));
	}
	//TFile *ifile = new TFile("/media/jeewon/Samsung_T5/Sr84/groundtest/data/20211203/ntp/ntp_test.root.00000");
	//TTree *itree = (TTree*)ifile->Get("tree");

	///// Histograms //////////////////////////////////////////////////////
	TH1D *h1_charge = new TH1D("h1_charge","SrI_{2} charge distribution;charge;counts", 10000, 0, 5000000);
	TH1D *h1_charge_Tl208 = new TH1D("h1_charge_Tl208","Tl208 peak;charge;counts",100,750000,950000);
	TH1D *h1_charge_K40 = new TH1D("h1_charge_K40","K40 peak;charge;counts",100,350000,650000);

	TH1D *h1_energy = new TH1D("h1_energy","SrI_{2} energy distribution;energy [keV];counts", 20000, 0, 20000);
	TH1D *h1_energy_K40 = new TH1D("h1_energy_K40","K40 peak;energy;counts",300,1300,1600);


	///// Energy calibration fitting /////////////////////////////////////
	double x[3] = {0.0};
	double y[3] = {0.0, 2614.7, 1460.8}; /// 0, Tl208, K40

	TCanvas *c1 = new TCanvas("c1","c1",1200,800);
	c1->Divide(2,2);
	c1->cd(1); /// ____________ charge distribution
	gPad->SetLogy();
	itree->Draw("crystal1_charge>>h1_charge","crystal1_charge>0");

	c1->cd(2); /// ____________ fitting Tl208 2.614 MeV peak 
	TF1 *f1 = new TF1("f1","gaus",820000,900000);
	itree->Fit("f1","crystal1_charge>>h1_charge_Tl208","crystal1_charge>0","R");
	h1_charge_Tl208->Draw();
	x[1] = f1->GetParameter(1);

	c1->cd(3); /// ____________ fitting K40 1.46 MeV peak
	//gPad->SetLogy();
	TF1 *f2 = new TF1("f2","gaus",460000,510000);
	itree->Fit("f2","crystal1_charge>>h1_charge_K40","crystal1_charge>0","R");
	h1_charge_K40->Draw();
	x[2] = f2->GetParameter(1);

	c1->cd(4); /// ____________ energy calibration function
	TF1 *f3 = new TF1("f3","pol1");
	TGraph *g1 = new TGraph(3,x,y);
	g1->SetMarkerStyle(22);
	g1->SetMarkerColor(kBlue);
	g1->SetTitle("energy calibration");
	g1->Fit("f3");
	g1->Draw("ap");

	double b = f3->GetParameter(0);
	double a = f3->GetParameter(1);
	TLatex *lt1 = new TLatex(400000,200,Form("y = %fx + %f",a,b));
	lt1->Draw();
	lt1->DrawLatex(400000, 400, "calibration funtion");


	///// Calculate energy resolution ////////////////////////////////////
	TCanvas *c2 = new TCanvas("can1", "can1", 1200, 800);
	c2->Divide(2,2);
	c2->cd(1); /// _____________ energy distribution after calibration
	gPad->SetLogy();
	itree->Draw(Form("crystal1_charge*%f+%f >> h1_energy",a,b),"crystal1_charge>0");
	h1_energy->DrawCopy();
	c2->cd(2); /// _____________ zoomed plot
	gPad->SetLogy();
	h1_energy->SetAxisRange(0,3000);
	h1_energy->DrawCopy();
	c2->cd(3); /// _____________ fitting K40 1.46 MeV peak
	TF1 *f4 = new TF1("f4","gaus",1400,1520);
	itree->Fit("f4",Form("crystal1_charge*%f+%f >> h1_energy_K40",a,b),"crystal1_charge>0","R");
	h1_energy_K40->Draw();

	double FWHM = 2.35*f4->GetParameter(2);
	double resol = FWHM/y[2]*100;
	TLatex *lt2 = new TLatex(1310,90,Form("FWHM = %f",FWHM));
	lt2->Draw();
	lt2->DrawLatex(1310,80,Form("(%f %% resolution)",resol));


	///// Printout the event number on K40 energy
	itree->Scan("EventNumber:SubrunNumber",Form(" 1450 < crystal1_charge*%f+%f && crystal1_charge*%f+%f<1470",a,b,a,b));
	//itree->Scan("EventNumber:SubrunNumber",Form(" 2600 < crystal1_charge*%f+%f && crystal1_charge*%f+%f<2620",a,b,a,b));
	//itree->Scan("EventNumber:SubrunNumber",Form(" 4995 < crystal1_charge*%f+%f && crystal1_charge*%f+%f<5005",a,b,a,b));

	/*
	int evtNo,fileNo,trgpoint,maxvalue,maxx,saturate;
	double charge,meantime,ped_mean,ped_rms,rped_mean,rped_rms,Npeak,peakx,peakh;
	
	itree->SetBranchAddress("EventNumber",&evtNo);
	itree->SetBranchAddress("SubrunNumber",&fileNo);
	itree->SetBranchAddress("crystal1_trgpoint",&trgpoint);
	itree->SetBranchAddress("crystal1_maxvalue",&maxvalue);
	itree->SetBranchAddress("crystal1_maxx",&maxx);
	itree->SetBranchAddress("crystal1_saturate",&saturate);
	itree->SetBranchAddress("crystal1_charge",&charge);
	itree->SetBranchAddress("crystal1_meantime",&meantime);
	itree->SetBranchAddress("crystal1_ped_mean",&ped_mean);
	itree->SetBranchAddress("crystal1_ped_rms",&ped_rms);
	itree->SetBranchAddress("crystal1_rped_mean",&rped_mean);
	itree->SetBranchAddress("crystal1_rped_rms",&rped_rms);
	itree->SetBranchAddress("crystal1_Npeak",&Npeak);
	itree->SetBranchAddress("crystal1_peakx",&peakx);
	itree->SetBranchAddress("crystal1_peakh",&peakh);

	///// Event Loop //////////////////////////////////////////////////////
	cout << "total event : " << itree->GetEntries() << endl;
	for(int ievt = 0; ievt < itree->GetEntries(); ievt++){
		itree->GetEntry(ievt);
		double energy = charge*a+b;
		if(energy){ 
		}
	}
	*/
}

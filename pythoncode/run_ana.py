import numpy as np
import matplotlib.pyplot as plt 
import sys, glob
from ROOT import TFile

import analysis_function as af

run = sys.argv[1]
hv = sys.argv[2]
ch = int(sys.argv[3])

charge, meantime = af.get_data(run, hv)
af.draw_meantimeVScharge(charge, meantime, ch)


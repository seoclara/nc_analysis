import numpy as np
import matplotlib.pyplot as plt
import sys, glob
from ROOT import TFile

xmax = [200e3,200e3,200e3]

def get_data(irun, hv):
    #ipath = f'/run/media/jeewon/Samsung_T5/Sr84/data/{irun}/ntp-1'
    ipath = f'/Volumes/Samsung_T5/Sr84/data/{irun}/ntp-1'
    ifile = TFile(ipath+'/ch1_ch2_Na22_{}V.root'.format(hv))
    itree = ifile.tree
    nevt = itree.GetEntries()
    print(nevt)

    charge = np.zeros((3,0))
    meantime = np.zeros((3,0))

    for ievt in itree:
        #print(ievt.crystal1_charge)
        now_charge = np.array([ievt.crystal1_charge, ievt.crystal2_charge, ievt.crystal3_charge])
        now_meantime = np.array([ievt.crystal1_meantime, ievt.crystal2_meantime, ievt.crystal3_meantime])
        charge = np.append(charge, now_charge.reshape(3,1), axis=1)
        meantime = np.append(meantime, now_meantime.reshape(3,1), axis=1)

    return charge, meantime

def get_resolution(charge, ich):
    plt.figure(figsize=(12,6))
    plt.subplot(121)
    plt.hist(charge[0], bins = 200, range=(0,xmax[0]), log= True, histtype='step')
    plt.subplot(122)
    plt.hist(charge[1],histtype='step')
    plt.hist(charge[2],histtype='step')
    plt.show()

def draw_meantimeVScharge(charge, meantime, ich):
    plt.figure()
    plt.scatter(charge[ich], meantime[ich], marker='.')
    plt.xlim(0,xmax[ich])
    plt.show()



{
	gROOT->SetStyle("Plain");

	int a=3;
	double en[3] = {609, 1460, 2614};
	double den[3] = {};


double hpge1[3] = {4148, 108585, 197310};
double hpge2[3] = {4307, 111778, 202922};
double hpge3[3] = {3830, 101079, 184194};

double dhpge1[3] = {499, 4375, 6404};
double dhpge2[3] = {419, 3382, 4701};
double dhpge3[3] = {446, 4480, 6260};



//	TGraph *ch1 = new TGraphErrors(a,hpge1,en,dhpge1,den);
//	TGraph *ch2 = new TGraphErrors(a,hpge2,en,dhpge2,den);
//	TGraph *ch3 = new TGraphErrors(a,hpge3,en,dhpge3,den);
	TGraph *ch1 = new TGraphErrors(a,en, hpge1,den,dhpge1);
	TGraph *ch2 = new TGraphErrors(a,en, hpge2,den,dhpge2);
	TGraph *ch3 = new TGraphErrors(a,en, hpge3,den,dhpge3);


	TMultiGraph *mg = new TMultiGraph();

	ch1->SetMarkerStyle(25);
	ch1->SetMarkerSize(1.5);
	ch1->GetYaxis()->SetTitle("Energy(keV)");
	ch1->GetXaxis()->SetTitle("# of Channel");
	ch1->SetTitle("detector 1");
//	ch1->GetYaxis()->SetRangeUser(0, 4000);
//	ch1->GetXaxis()->SetLimits(0, 300000);

	ch2->SetMarkerStyle(25);
	ch2->SetMarkerSize(1.5);
	ch2->GetYaxis()->SetTitle("Energy(keV)");
	ch2->GetXaxis()->SetTitle("# of Channel");
	ch2->SetTitle("detector 2");
//	ch2->GetYaxis()->SetRangeUser(0, 4000);
//	ch2->GetXaxis()->SetLimits(0, 300000);

	ch3->SetMarkerStyle(25);
	ch3->SetMarkerSize(1.5);
	ch3->GetYaxis()->SetTitle("Energy(keV)");
	ch3->GetXaxis()->SetTitle("# of Channel");
	ch3->SetTitle("detector 3");
//`	ch3->GetYaxis()->SetRangeUser(0, 4000);
//	ch3->GetXaxis()->SetLimits(0, 300000);

	cout << "\n below linear equation parameters are from ch1 ~ ch3\n"<< endl;

	TCanvas *fith = new TCanvas("fit", "fit", 1000, 1000);

	fith->Divide(2,2);

	fith->cd(1);
	mg->Add(ch1);
	ch1->Fit("pol1");
	ch1->Draw();

	fith->cd(2);
	mg->Add(ch2);
	ch2->Fit("pol1");
	ch2->Draw();

	fith->cd(3);
	mg->Add(ch3);
	ch3->Fit("pol1");
	ch3->Draw();

	double p0[3], p1[3];

	TF1 *fit1 = ch1->GetFunction("pol1");
	p0[0] = fit1->GetParameter(0); //pol0
	p1[0] = fit1->GetParameter(1); //pol1
	printf("p0[1] = %.6f;		p1[1] = %.6f;	\n", p0[0], p1[0]);

	TF1 *fit2 = ch2->GetFunction("pol1");
	p0[1] = fit2->GetParameter(0); //pol0
	p1[1] = fit2->GetParameter(1); //pol1
	printf("p0[2] = %.6f;		p1[2] = %.6f;	\n", p0[1], p1[1]);

	TF1 *fit3 = ch3->GetFunction("pol1");
	p0[2] = fit3->GetParameter(0); //pol0
	p1[2] = fit3->GetParameter(1); //pol1
	printf("p0[3] = %.6f;		p1[3] = %.6f;	\n", p0[2], p1[2]);

}

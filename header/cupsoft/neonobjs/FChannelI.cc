/*
 *
 *  Module:  FChannelI/RawObjs
 *
 *  Author:  Jaison Lee
 *
 *  Purpose: A Class for storing FADC digitzed waveform
 *
 *  Last Update:      $Author: cupsoft $
 *  Update Date:      $Date: 2020/01/14 02:26:08 $
 *  CVS/RCS Revision: $Revision: 1.3 $
 *  Status:           $State: Exp $
 *
 */

#include <iostream>

#include "TMath.h"
#include "TH1D.h"

#include "FChannelI.hh"

ClassImp(FChannelI)

FChannelI::FChannelI()
: AbsChannel(), ArrayI()
{
  fWaveHis  = NULL;
  fWaveHisC  = NULL;
  fPedestal = 0;
}

FChannelI::FChannelI(Int_t id, Int_t ndp)
  : AbsChannel(id), ArrayI(ndp)
{
  fWaveHis = NULL;
  fWaveHisC = NULL;
  fPedestal = 0;
}

FChannelI::FChannelI(Int_t id, Int_t ndp, const unsigned int * wave)
  : AbsChannel(id), ArrayI(ndp, wave)
{
  fWaveHis = NULL;
  fWaveHisC = NULL;
  fPedestal = 0;
}

FChannelI::FChannelI(const FChannelI & ch)
  : AbsChannel(ch), ArrayI(ch)
{
  fWaveHis = NULL;
  fWaveHisC = NULL;
  fPedestal = ch.GetPedestal();
}

FChannelI::~FChannelI()
{
  if(fWaveHis != NULL) delete fWaveHis;
  if(fWaveHisC != NULL) delete fWaveHisC;
}

void FChannelI::SetNdp(Int_t ndp)
{
  Set(ndp);
}

void FChannelI::SetWaveform(const unsigned int * wave)
{
  if(GetSize() == 0){
    Error("SetWaveform", "Number of data point (ndp) is not set");
    return;
  }
  
  Int_t n = GetSize();
  Set(n, wave);
}

void FChannelI::SetWaveform(Int_t ndp, const unsigned int * wave)
{
  Set(ndp, wave);
}


void FChannelI::SetWaveform(Int_t n, unsigned int count)
{
  if(GetSize() == 0){
    Error("SetWaveform", "Number of data point (ndp) is not set");
    return;
  }

  AddAt(count, n);
}

Int_t FChannelI::GetNdp() const      
{ 
  return GetSize();
}

const unsigned int * FChannelI::GetWaveform() const
{ 
  return GetArray(); 
}

TH1D * FChannelI::GetWaveformHist(Double_t pedm)
{
  int ndp = GetSize();

  if(!fWaveHis){
    fWaveHis = new TH1D(Form("Waveform_ch%03d", fID),"", ndp, 0, ndp);
  }

  if(pedm == 0){
    pedm = fPedestal;
  }

  fWaveHis->Reset();
  for(Int_t i = 1; i <= ndp; i++){
    fWaveHis->SetBinContent(i, At(i-1)-pedm);
  }

  return fWaveHis;
}


/**
$Log: FChannelI.cc,v $
Revision 1.3  2020/01/14 02:26:08  cupsoft
*** empty log message ***

Revision 1.2  2017/03/20 05:41:55  ysy
.

Revision 1.1.1.1  2016/07/14 07:58:51  cupsoft
RawObjs

Revision 1.3  2016/03/17 08:12:41  jslee
add inheritance from TArray class
remove member array for waveform

Revision 1.2  2016/03/08 04:40:33  amore
*** empty log message ***

Revision 1.1.1.1  2016/02/29 08:25:13  cupsoft
RawObjs

**/

#include "TGClient.h"

#include "RACAbsDisplay.hh"

ClassImp(RACAbsDisplay)

RACAbsDisplay::RACAbsDisplay(const char * name)
: TGMainFrame(gClient->GetRoot(), 100, 100),
  fDisplayName(name)
{

}

RACAbsDisplay::~RACAbsDisplay()
{

}

void RACAbsDisplay::ShowDisplay()
{
  SetWindowName(fDisplayName);
  MapSubwindows();
  Resize(GetDefaultSize());
  MapWindow();
}

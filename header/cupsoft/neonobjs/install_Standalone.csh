#!/bin/csh -f

if(0) then
###########
mkdir cupsoft;
cd cupsoft;
endif

if(0) then
# install cosine obj
git clone git@gitlab.com:neon_cnns/neonobjs.git
cd neonprod
make -j 20
echo "============================="
make clean
echo "============================="
make -j 20
cd ..
endif

if(1) then
git clone git@gitlab.com:neon_cnns/neonprod.git
endif

if(1) then

cd neonprod
ln -s ../neonobjs/libNeonObjs* .
ln -s ../neonobjs/*.hh .
mkdir Framework
mkdir RawObjs
cd Framework/
mkdir framework
mkdir objects
cd framework/
ln -s ../../RACRootManager.hh
ln -s ../../RACRootModule.hh
cd ../objects/
ln -s ../../AbsEvent.hh .
cd ../../RawObjs/
ln -s ../EventInfo.hh
ln -s ../AChannel.hh .
ln -s ../AChannelData.hh .
ln -s ../FChannelS.hh .
ln -s ../FChannelSData.hh .
cd ../
ln -s src/* .
ln -s NeonMods/NeonProduction.hh .
ln -s NeonMods/PulseProduction.hh .

make -f Makefile -j 20
endif

#ifndef ADCHeader_hh
#define ADCHeader_hh

#include "TObject.h"

class ADCHeader : public TObject {
public:
  ADCHeader();
  ADCHeader(const ADCHeader & header);
  virtual ~ADCHeader();

  bool error;
  bool zero[32];

  unsigned short mid;
  unsigned short cid;
  unsigned short ttype;
  unsigned short tdest;

  unsigned int runnum;
  unsigned int dlen;
  unsigned int ped[32];
  unsigned int tnum;
  unsigned int ctnum;
  unsigned int ctptn;
  unsigned long ttime;
  unsigned long cttime;

  void Dump() const;

ClassDef(ADCHeader, 1)
};

#endif
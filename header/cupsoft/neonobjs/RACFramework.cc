//------------------------------------------------------------------------
// File :
//      RACFramework.cc
//
// Description:
//      Class RACFramework is the main class for RAC
//      framework. 
//
// Author List:
//      Jaison Lee                Original Author
//
// Jan-30-2007 (Jaison Lee) : RACFramework start
// Nov-25-2013 (Jaison Lee) : modified for general framework
//
// Last Update:      $Author: cupsoft $
// Update Date:      $Date: 2018/11/20 07:54:48 $
// CVS/RCS Revision: $Revision: 1.23 $
// Status:           $State: Exp $
//------------------------------------------------------------------------

#include <iostream>
#include <stdlib.h>

#include "TROOT.h"
#include "TClass.h"

#include "RACFramework.hh"
#include "RACStdInputModule.hh"

using namespace std;

ClassImp(RACFramework)

RACFramework::RACFramework()
  :TNamed("RACFramework", "Analysis Control Main Frame")
{
  Init();

  cout<<endl;
  cout<<"================================================================"<<endl;
  cout<<"            RACFramework Initialized done ......                "<<endl;
  cout<<"================================================================"<<endl;
  cout<<endl;
}

RACFramework::~RACFramework()
{
  UInit();

  cout<<endl;
  cout<<"================================================================"<<endl;
  cout<<"            RACFramework Ended ......                           "<<endl;
  cout<<"================================================================"<<endl;
  cout<<endl;
}

void RACFramework::Init()
{
  fAbsEvent        = AbsEvent::Instance();
  fModuleList      = new TList();

  fInputModule = NULL;

  fOutputModule = new RACStdOutputModule();
  fOutputModule->SetFramework(this);
  fOutputModule->SetDisable();

  fRootManager = new RACUserRootModule();
  fRootManager->SetFramework(this);
  fRootManager->SetDisable();

  fDBManager = RACDBManager::Instance();
  fDBManager->SetFramework(this);

  fIsFirstRun   = true;
  fIsFirstEvent = true;

  fNInputReport = 100;
  fRunNumber    =   0;

  fProcessedEvent = 0;
  fUserRootFileUpdateRate = 0;
}

void RACFramework::UInit()
{
  fAbsEvent->DeleteInstance();

  fModuleList->Delete();
  delete fModuleList;

  if(fInputModule) delete fInputModule ;
  delete fOutputModule;
  delete fRootManager ;

  fDBManager->DeleteInstance();
}

bool RACFramework::BeginJob()
{
  if(fInputModule == NULL)
    Warning("BeginJob","No InputModule defined ......");

  cout<<endl;
  cout<<"******************************************************************"<<endl;
  cout<<"     RACFramework Module List"<<endl;
  cout<<"     ========================"<<endl;
  cout<<endl;
  cout<<setw(12)<<"Type"<<setw(24)<<"Name"<<setw(12)<<"Enabled"<<endl;
  if(fInputModule) fInputModule->Report();
  fRootManager->Report();
  TIter it(fModuleList);
  while(RACModule * mod = (RACModule*)it.Next())
    mod->Report();
  fOutputModule->Report();
  cout<<"******************************************************************"<<endl;
  cout<<endl;

  if(fInputModule){
    fInputModule->SetNReport(fNInputReport);
    if(!fInputModule->BeginJob(fAbsEvent)) 
      return false;
  }

  if(fRootManager->IsEnabled()){
    if(!fRootManager->BeginJob(fAbsEvent))
      return false;
  }

  if(!fDBManager->BeginJob(fAbsEvent)) 
    return false;

  TIter it1(fModuleList);
  while(RACModule * mod = (RACModule*)it1.Next()){
    if(mod->IsEnabled()){
      if(!mod->BeginJob(fAbsEvent)) 
	return false;
    }
  }

  if(fOutputModule->IsEnabled()){
    if(!fOutputModule->BeginJob(fAbsEvent)) 
      return false;
  }

  return true;
}

bool RACFramework::EndJob()
{
  if(fInputModule){
    if(!fInputModule->EndJob(fAbsEvent)) 
      return false;
  }

  if(!fDBManager->EndJob(fAbsEvent))
    return false;

  TIter it(fModuleList);
  while(RACModule * mod = (RACModule*)it.Next()){
    if(mod->IsEnabled()){
      if(!mod->EndJob(fAbsEvent)) 
	return false;
    }
  }

  if(fRootManager->IsEnabled()){
    if(!fRootManager->EndJob(fAbsEvent))
      return false;
  }

  if(fOutputModule->IsEnabled()){
    if(!fOutputModule->EndJob(fAbsEvent))
      return false;
  }


  double cputime  = 0;
  double realtime = 0;

  cout<<endl;
  cout<<endl;
  cout<<"**************************************************************************"<<endl;
  cout<<"     RACFramework EndJob Report"<<endl;
  cout<<"     =========================="<<endl;
  cout<<endl;
  cout<<setw(20)<<"Name"
      <<setw(10)<<"CPU[s]"
      <<setw(10)<<"Real[s]"
      <<setw(14)<<"CPU[us/evt]"
      <<setw(14)<<"Real[us/evt]"
      <<endl;

  if(fInputModule){
    fInputModule->ReportTime(fProcessedEvent);
    cputime += fInputModule->GetCPUTime();
    realtime += fInputModule->GetRealTime();
  }

  TIter it1(fModuleList);
  while(RACModule * mod = (RACModule*)it1.Next()){
    mod->ReportTime(fProcessedEvent);
    cputime += mod->GetCPUTime();
    realtime += mod->GetRealTime();
  }

  fOutputModule->ReportTime(fProcessedEvent);
  cputime += fOutputModule->GetCPUTime();
  realtime += fOutputModule->GetRealTime();
  cout<<"----------------------------------------------------------------------"<<endl;
  cout<<setw(20)<<"Total"
      <<setw(10)<<Form("%10.1f",cputime)
      <<setw(10)<<Form("%10.1f",realtime)
      <<setw(14)<<Form("%10.0f",1000000*cputime/fProcessedEvent)
      <<setw(14)<<Form("%10.0f",1000000*realtime/fProcessedEvent)
      <<endl;
  cout<<"**************************************************************************"<<endl;
  cout<<endl;



  return true;
}

bool RACFramework::BeginRun()
{
  if(!fDBManager->BeginRun(fAbsEvent))
    return false;

  TIter it(fModuleList);
  while(RACModule * mod = (RACModule*)it.Next()){
    if(mod->IsEnabled()){
      if(!mod->BeginRun(fAbsEvent))
	return false;
    }
  }

  return true;
}

bool RACFramework::EndRun()
{
  if(fInputModule){
    if(!fInputModule->EndRun(fAbsEvent))
      return false;
  }

  if(!fDBManager->EndRun(fAbsEvent))
    return false;

  TIter it(fModuleList);
  while(RACModule * mod = (RACModule*)it.Next()){
    if(mod->IsEnabled()){
      if(!mod->EndRun(fAbsEvent))
	return false;
    }
  }

  return true;
}

bool RACFramework::Run(int nev, int nskip)
{
  if(!BeginJob())
    return false;
  
  if(!BeginRun())
    return false;

  Long64_t nTotalEvent = (fInputModule != NULL) ? fInputModule->GetEntries() : 10000000000;

  Long64_t nProcessingEvent;
  if(nev == 0)
    nProcessingEvent = nTotalEvent;
  else{
    if(nev > nTotalEvent){
      Warning("Run", 
	      Form("Processing event (%d) is bigger than total event (%lld), total event will be processed", nev, nTotalEvent));
      nProcessingEvent = nTotalEvent;
    }
    else
      nProcessingEvent = nev;
  }

  for(Long64_t i = 0; i < nProcessingEvent; i += nskip+1  ) {
    if(!ProcessEvent(i)) break;
  }

  if(!EndRun())
    return false;
    
  if(!EndJob())
    return false;

  return true;
}

bool RACFramework::ProcessEvent(int iEntry)
{
  int nb = 0;
  if(fInputModule){
    fInputModule->StartStopwatch();
    nb = fInputModule->GetEntry(iEntry);
    fInputModule->StopStopwatch();
  }

  if(nb == 0 && fInputModule){
    Info("ProcessEvent", "No more entry in input");
    return false;
  }

  bool writeEvent = true;

  TIter it(fModuleList);
  while(RACModule * mod = (RACModule*)it.Next()){
    if(mod->IsEnabled()){
      mod->StartStopwatch();
      mod->SetPassed(false);
      if(!mod->Event(fAbsEvent))
        return false;
      if(mod->IsPassed()){
        writeEvent = false;
        break;
      }
      mod->StopStopwatch();
    }
  }

  if(fRootManager->IsEnabled() && fUserRootFileUpdateRate) {
    if (fProcessedEvent % fUserRootFileUpdateRate == 0)
      RACRootManager::Instance()->Update();
  }

  if(fOutputModule->IsEnabled() && writeEvent){
    fOutputModule->StartStopwatch();
    if(!fOutputModule->Event(fAbsEvent))
      return false;
    fOutputModule->StopStopwatch();
  }

  fProcessedEvent += 1;

  return true;
}


void RACFramework::AddModule(RACModule * aMod)
{
  aMod->SetFramework(this);
  fModuleList->Add(aMod);
}

RACModule * RACFramework::AddModule(const char * clName, const char * modName, const char * title)
{
  RACModule * m = 0;
  
  TClass * cl = gROOT->GetClass(clName);
  if(!cl){
    Error("RACFramework::AddModule","class %s is not known to the interpreter",clName);
  } else {
    m = (RACModule*)cl->New();
    if(modName) m->SetName(modName);
    if(title)   m->SetTitle(title);

    m->SetFramework(this);
    fModuleList->Add(m);
  }
  
  return m;
}

int RACFramework::DropInputData(const char * dataName, const char * prodName)
{
  return fAbsEvent->AddDropInputData(dataName, prodName);
}

int RACFramework::DropOutputData(const char * dataName, const char * prodName)
{
  return fAbsEvent->AddDropOutputData(dataName, prodName);
}

int RACFramework::AddStdDataInputFile(const char * fileName)
{
  if(fInputModule == NULL){
    fInputModule = new RACStdInputModule();
    fInputModule->SetFramework(this);
  }

  return fInputModule->AddFile(fileName);
}

int RACFramework::SetOutputFile(const char * fileName)
{
  fOutputModule->SetFileName(fileName);
  fOutputModule->SetEnable();

  return 0;
}

void RACFramework::SetOutputFileMaxSize(int val)
{
  fOutputModule->SetMaxFileSize(val);
}

void RACFramework::SetUserRootFileUpdateRate(int nevt)
{
  fUserRootFileUpdateRate = nevt;
}

int RACFramework::SetUserRootFile(const char * fileName, bool isMemFile)
{
  fRootManager->SetUserRootFileName(fileName);
  fRootManager->SetEnable();

  if(isMemFile){
    RACRootManager::Instance()->SetMemFile();
  }

  return 0;
}

/**
$Log: RACFramework.cc,v $
Revision 1.23  2018/11/20 07:54:48  cupsoft
*** empty log message ***

Revision 1.22  2018/09/21 04:08:52  cupsoft
*** empty log message ***

Revision 1.21  2017/09/14 07:01:46  cupsoft
*** empty log message ***

Revision 1.20  2016/07/14 07:07:03  cupsoft
*** empty log message ***

Revision 1.19  2015/04/14 11:22:29  yjko
*** empty log message ***

Revision 1.18  2015/03/23 03:58:02  jslee
bug fixed, read tree size

Revision 1.17  2015/03/19 05:03:21  jslee
change C++ code style

**/

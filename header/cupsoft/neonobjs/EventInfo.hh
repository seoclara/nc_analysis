#ifndef EventInfo_HH
#define EventInfo_HH

#include "TClonesArray.h"

#include "AbsEventInfo.hh"
#include "ADCHeader.hh"
#include "ADCEventHeader.hh"

class EventInfo : public AbsEventInfo {
public:
  EventInfo();
  EventInfo(const EventInfo & info);
  virtual ~EventInfo();

  void AddADCHeader(ADCHeader * header);
  void AddADCHeader(ADCEventHeader * header);
  void CopyFrom(const EventInfo * info);

  TClonesArray * GetADCHeaders() const;
  int GetNADCHeader() const;
  ADCEventHeader * GetADCHeader(int mid) const;

  unsigned int GetLocalTriggerNumber(int mid) const;
  unsigned long GetLocalTriggerTime(int mid) const;

  virtual void Clear(const Option_t * opt = "");
  virtual void Dump() const;

private:
  Int_t fNADCHeader;//! just for counter
  TClonesArray * fADCHeaders;

ClassDef(EventInfo, 1)
};

inline TClonesArray * EventInfo::GetADCHeaders() const
{
  return fADCHeaders;
}

inline int EventInfo::GetNADCHeader() const
{
  return fADCHeaders->GetEntries();
}

#endif

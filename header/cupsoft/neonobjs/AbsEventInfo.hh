#ifndef AbsEventInfo_hh
#define AbsEventInfo_hh

#include "AbsData.hh"

class AbsEventInfo : public AbsData {
public:
  AbsEventInfo();
  AbsEventInfo(const AbsEventInfo & evt);
  virtual ~AbsEventInfo();

  void SetRunNumber(unsigned int val);
  void SetEventNumber(unsigned int val);
  void SetTriggerType(unsigned int val);
  void SetTriggerNumber(unsigned int val);
  void SetTriggerTime(unsigned long val);

  unsigned int GetRunNumber() const;
  unsigned int GetEventNumber() const;
  unsigned int GetTriggerType() const;
  unsigned int GetTriggerNumber() const;
  unsigned long GetTriggerTime() const;

protected:
  unsigned int fRunNumber;
  unsigned int fEventNumber;
  unsigned int fTriggerType;
  unsigned int fTriggerNumber;
  unsigned long fTriggerTime;

ClassDef(AbsEventInfo, 1)
};

inline void AbsEventInfo::SetRunNumber(unsigned int val)
{
  fRunNumber = val;
}

inline void AbsEventInfo::SetEventNumber(unsigned int val)
{
  fEventNumber = val;
}

inline void AbsEventInfo::SetTriggerType(unsigned int val)
{
  fTriggerType = val;
}

inline void AbsEventInfo::SetTriggerNumber(unsigned int val)
{
  fTriggerNumber = val;
}

inline void AbsEventInfo::SetTriggerTime(unsigned long val)
{
  fTriggerTime = val;
}

inline unsigned int AbsEventInfo::GetRunNumber() const
{
  return fRunNumber;
}

inline unsigned int AbsEventInfo::GetEventNumber() const
{
  return fEventNumber;
}

inline unsigned int AbsEventInfo::GetTriggerType() const
{
  return fTriggerType;
}

inline unsigned int AbsEventInfo::GetTriggerNumber() const
{
  return fTriggerNumber;
}

inline unsigned long AbsEventInfo::GetTriggerTime() const
{
  return fTriggerTime;
}

#endif

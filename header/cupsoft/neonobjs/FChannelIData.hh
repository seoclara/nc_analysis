/*
 *
 *  Module:  FChannelIData/RawObjs
 *
 *  Author:  Jaison Lee
 *
 *  Purpose: TClonesArray for FChannelIs
 *
 *  Last Update:      $Author: cupsoft $
 *  Update Date:      $Date: 2020/01/14 02:26:08 $
 *  CVS/RCS Revision: $Revision: 1.2 $
 *  Status:           $State: Exp $
 *
 */

#ifndef FChannelIData_hh
#define FChannelIData_hh

#include "TClonesArray.h"

#include "AbsData.hh"

typedef TClonesArray FChannelColl;

class FChannelI;
class FChannelIData : public AbsData 
{
public:
  FChannelIData();
  FChannelIData(const FChannelIData & data);
  virtual ~FChannelIData();
  
  virtual void Clear(const Option_t * opt = "");
  virtual void Dump() const;
  
  FChannelI * Add();
  FChannelI * Add(Int_t id, Int_t ndp);
  FChannelI * Add(Int_t id, Int_t ndp, const unsigned int * wave);
  FChannelI * Add(FChannelI * ch);

  FChannelColl * GetColl()    const;
  Int_t          GetN()       const;
  FChannelI    * Get(Int_t n) const;

  FChannelI    * GetChannel(Int_t id) const;

private:
  Int_t          fNCh ;//! just for counter
  FChannelColl * fColl;
  
  ClassDef(FChannelIData, 1)
};

//
// Inline functions
//

inline FChannelColl * FChannelIData::GetColl() const
{
  return fColl; 
}

inline Int_t FChannelIData::GetN() const
{
  return fColl->GetEntries(); 
}

inline FChannelI * FChannelIData::Get(Int_t n) const
{
  return (FChannelI*)fColl->At(n); 
}

#endif

/**
$Log: FChannelIData.hh,v $
Revision 1.2  2020/01/14 02:26:08  cupsoft
*** empty log message ***

Revision 1.1.1.1  2016/07/14 07:58:51  cupsoft
RawObjs

Revision 1.2  2016/03/17 08:12:12  jslee
add inheritance from TArray class
remove member array for waveform

Revision 1.1.1.1  2016/02/29 08:25:13  cupsoft
RawObjs

**/

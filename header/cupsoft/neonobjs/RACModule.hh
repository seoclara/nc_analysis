//--------------------------------------------------------------------------
// File :
//      RACModule.hh
//
// Description:
//      Class RACModule is an abstract class to be inherited by any moudle 
//      which is supposed to be added into RAC (Reno Analysis Control) 
//      framework.
//
// Author List:
//      Jaison Lee                Original Author
//
// Jan-30-2007 (Jaison Lee) : RACModule start
// Nov-25-2013 (Jaison Lee) : modified for general framework
//
//------------------------------------------------------------------------
#ifndef RACModule_HH
#define RACModule_HH

//------------------------------------------------------------------------
// C and C++ header
//------------------------------------------------------------------------
#include <iostream>

//------------------------------------------------------------------------
// Utilities header
//------------------------------------------------------------------------
#include "TNamed.h"
#include "TString.h"
#include "TStopwatch.h"

//------------------------------------------------------------------------
// Collaboration header
//------------------------------------------------------------------------


class AbsEvent;
class RACFramework;
class RACRootManager;
class RACModule : public TNamed {
 public:
  enum TYPE {INPUT, OUTPUT, MANAGER, USER};

  RACModule(const char * name, const char * title);
  virtual ~RACModule();
  
  virtual bool BeginJob (AbsEvent * anEvent) = 0;
  virtual bool EndJob   (AbsEvent * anEvent) = 0;
  virtual bool BeginRun (AbsEvent * anEvent) = 0;
  virtual bool EndRun   (AbsEvent * anEvent) = 0;
  virtual bool Event    (AbsEvent * anEvent) = 0;
  
  // modifiers
  void SetEnable();
  void SetDisable();
  void SetPassed(bool pass);
  void SetFramework(RACFramework * framework);
  void SetType(TYPE type);
  void SetProdName(const char * name);

  void StartStopwatch();
  void StopStopwatch();


  // accessors
  bool IsEnabled() const;
  bool IsPassed() const;
  const char * GetProdName() const;
  RACFramework * GetFramework();

  double GetCPUTime() const;
  double GetRealTime() const;

  void Report() const;
  void ReportTime(int nevent) const;
  
protected:
  bool fIsEnable;
  bool fIsPassed;
  TYPE fType    ;

  TString fProdName;

  TStopwatch fStopwatch;

  double fCPUTime;
  double fRealTime;

  RACFramework * fFramework;

  ClassDef(RACModule, 0)
};

  // modifiers
inline void RACModule::SetEnable()
{
  fIsEnable = true;
}

inline void RACModule::SetDisable()
{
  fIsEnable = false;
}

inline void RACModule::SetPassed(bool pass)
{
  fIsPassed = pass;
}

inline void RACModule::SetProdName(const char * name)
{
  fProdName = name;
}


inline void RACModule::SetFramework(RACFramework * framework) 
{
  fFramework = framework; 
}
  
inline void RACModule::SetType(TYPE type) 
{
  fType = type; 
}
  
  // accessors
inline bool RACModule::IsEnabled() const
{
  return fIsEnable;
}

inline bool RACModule::IsPassed() const
{
  return fIsPassed;
}

inline const char * RACModule::GetProdName() const
{
  return fProdName; 
}

inline RACFramework * RACModule::GetFramework()
{
  return fFramework; 
}

inline void RACModule::StartStopwatch()
{
  fStopwatch.Start();
}

inline void RACModule::StopStopwatch()
{
  fStopwatch.Stop();

  fCPUTime  += fStopwatch.CpuTime();
  fRealTime += fStopwatch.RealTime();
}

inline double RACModule::GetCPUTime() const
{
  return fCPUTime;
}

inline double RACModule::GetRealTime() const
{
  return fRealTime;
}

#endif

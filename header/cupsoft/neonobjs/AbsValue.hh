//--------------------------------------------------------------------------
// File :
//      AbsValue.hh
//
// Description:
//      Template class AbsValue is for reading general tree
//
// Author List:
//      Jaison Lee                Original Author
//
// Jan-30-2007 (Jaison Lee) : AbsValue start
// Nov-25-2013 (Jaison Lee) : modified for general framework
//
//------------------------------------------------------------------------

#ifndef AbsValue_HH
#define AbsValue_HH

#include "TNamed.h"

template <class T>
class TValue : public TNamed {
public:
  TValue();  
  TValue(const char * name);
  virtual ~TValue() {}
  
  T   GetVal()         { return  _val; }
  T * GetAddress()     { return &_val; }
  
private:
  T _val; 

  ClassDef(TValue, 0)
};

template <class T>
class TValueArray : public TNamed {
public:
  TValueArray();  
  TValueArray(const char * name);
  virtual ~TValueArray() {}
  
  T   GetVal(int i)    { return  _val[i]; }
  T * GetAddress()     { return  _val   ; }
  
private:
  T _val[10000];  

  ClassDef(TValueArray, 0)  
};


template <class T>
class TValueArray2 : public TNamed {
public:
  TValueArray2();  
  TValueArray2(const char * name);
  virtual ~TValueArray2() {}

  
  T   GetVal(int i, int j, int k)  { return  _val[i*k+j]; }
  T * GetAddress()                 { return  _val       ; }
  
private:
  T _val[100000];  

  ClassDef(TValueArray2, 0)  
};


templateClassImp(TValue)
templateClassImp(TValueArray)
templateClassImp(TValueArray2)

template <class T>
TValue<T>::TValue()
  :TNamed()
{
  _val = 0;
}

template <class T>
TValue<T>::TValue(const char * name)
  :TNamed(name, "")
{
  _val = 0;
}

template <class T>
TValueArray<T>::TValueArray()
  :TNamed()
{
}

template <class T>
TValueArray<T>::TValueArray(const char * name)
  :TNamed(name, "")
{
}

template <class T>
TValueArray2<T>::TValueArray2()
  :TNamed()
{
}

template <class T>
TValueArray2<T>::TValueArray2(const char * name)
  :TNamed(name, "")
{
}

#endif

/*
 *
 *  Module:  AbsChannel/RawObjs
 *
 *  Author:  Jaison Lee
 *
 *  Purpose: An Abstract Class for storing basic channel information
 *
 *  Last Update:      $Author: cupsoft $
 *  Update Date:      $Date: 2016/07/14 07:58:51 $
 *  CVS/RCS Revision: $Revision: 1.1.1.1 $
 *  Status:           $State: Exp $
 *
 */

#ifndef AbsChannel_hh
#define AbsChannel_hh

#include "TObject.h"

class AbsChannel : public TObject 
{
public:
  AbsChannel();
  AbsChannel(Int_t id);
  AbsChannel(const AbsChannel & ch);

  virtual ~AbsChannel();
  
  virtual void SetID(UShort_t id);
  virtual void SetMID(UShort_t id);
  virtual void SetCID(UShort_t id);
  virtual void SetBit(UShort_t bit);
  
  virtual UShort_t GetID() const;
  virtual UShort_t GetMID() const;
  virtual UShort_t GetCID() const;
  virtual UShort_t GetBit() const;
  
protected:
  UShort_t fID ;// channel id
  UShort_t fMID;// module id
  UShort_t fCID;// module channel id
  UShort_t fBit;// trigger bit

  ClassDef(AbsChannel, 1)
};

//
// Inline functions
//

inline void AbsChannel::SetID(UShort_t id)
{
  fID = id;
}

inline void AbsChannel::SetMID(UShort_t id)
{
  fMID = id;
}

inline void AbsChannel::SetCID(UShort_t id)
{
  fCID = id;
}

inline void AbsChannel::SetBit(UShort_t bit)
{
  fBit = bit;
}

inline UShort_t AbsChannel::GetID() const
{ 
  return fID;
}

inline UShort_t AbsChannel::GetMID() const
{ 
  return fMID;
}

inline UShort_t AbsChannel::GetCID() const
{ 
  return fCID;
}

inline UShort_t AbsChannel::GetBit() const      
{ 
  return fBit;
}

#endif

/**
$Log: AbsChannel.hh,v $
Revision 1.1.1.1  2016/07/14 07:58:51  cupsoft
RawObjs

Revision 1.1.1.1  2016/02/29 08:25:13  cupsoft
RawObjs

**/

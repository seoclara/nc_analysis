#ifndef ADCEventHeader_hh
#define ADCEventHeader_hh

#include "TObject.h"

#include "ADCHeader.hh"

class ADCEventHeader : public TObject {
public:
  ADCEventHeader();
  ADCEventHeader(const ADCHeader & header);
  ADCEventHeader(const ADCEventHeader & header);
  virtual ~ADCEventHeader();

  unsigned short GetMID() const;
  unsigned int GetTriggerNumber() const;
  unsigned long GetTriggerTime() const;

  void Dump() const;

private:
  unsigned short mid;
  unsigned int ctnum; // local trigger number
  unsigned long cttime; // local trigger time

ClassDef(ADCEventHeader, 2)
};

inline unsigned short ADCEventHeader::GetMID() const
{
  return mid;
}

inline unsigned int ADCEventHeader::GetTriggerNumber() const
{
  return ctnum;
}

inline unsigned long ADCEventHeader::GetTriggerTime() const
{
  return cttime;
}

#endif

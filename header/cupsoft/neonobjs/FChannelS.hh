/*
 *
 *  Module:  FChannelS/RawObjs
 *
 *  Author:  Jaison Lee
 *
 *  Purpose: A Class for storing digitzed waveform
 *
 *  Last Update:      $Author: cupsoft $
 *  Update Date:      $Date: 2020/01/14 02:26:08 $
 *  CVS/RCS Revision: $Revision: 1.2 $
 *  Status:           $State: Exp $
 *
 */

#ifndef FChannelS_hh
#define FChannelS_hh

#include <vector>

#include "ArrayS.hh"
#include "AbsChannel.hh"

class TH1D;
class FChannelS : public AbsChannel, public ArrayS 
{
public:
  FChannelS();
  FChannelS(Int_t id);
  FChannelS(Int_t id, Int_t ndp);
  FChannelS(Int_t id, Int_t ndp, const unsigned short * wave);
  FChannelS(const FChannelS & ch);

  virtual ~FChannelS();
  
  void SetNdp(Int_t ndp);
  void SetWaveform(const unsigned short * wave);
  void SetWaveform(Int_t ndp, const unsigned short * wave);
  void SetWaveform(Int_t n, unsigned short count);
  void SetPedestal(Int_t ped);

  Int_t GetNdp() const;
  unsigned short GetPedestal() const;
  const unsigned short * GetWaveform() const;

  TH1D * GetWaveformHist(double pedm = 0);
  
protected:
  unsigned short  fPedestal;// pedestal from FADC
  TH1D * fWaveHis ;//! just for drawing waveform
  
  ClassDef(FChannelS, 1)
};

//
// Inline functions
//
inline void FChannelS::SetPedestal(Int_t ped)
{
  fPedestal = ped;
}

inline unsigned short FChannelS::GetPedestal() const
{ 
  return fPedestal;
}

#endif

/**
$Log: FChannelS.hh,v $
Revision 1.2  2020/01/14 02:26:08  cupsoft
*** empty log message ***

Revision 1.1.1.1  2016/07/14 07:58:51  cupsoft
RawObjs

Revision 1.3  2016/03/17 08:12:11  jslee
add inheritance from TArray class
remove member array for waveform

Revision 1.2  2016/03/08 04:40:33  amore
*** empty log message ***

Revision 1.1.1.1  2016/02/29 08:25:13  cupsoft
RawObjs

**/

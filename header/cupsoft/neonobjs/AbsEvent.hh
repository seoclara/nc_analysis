//--------------------------------------------------------------------------
// File :
//      AbsEvent.hh
//
// Description:
//      Class AbsEvent is the main class for an event for framework
//
// Author List:
//      Jaison Lee                Original Author
//
// Jan-30-2007 (Jaison Lee) : AbsEvent start
// Nov-25-2013 (Jaison Lee) : modified for general framework
//
// Last Update:      $Author: cupsoft $
// Update Date:      $Date: 2018/11/20 07:54:48 $
// CVS/RCS Revision: $Revision: 1.13 $
// Status:           $State: Exp $
//------------------------------------------------------------------------

#ifndef AbsEvent_HH
#define AbsEvent_HH

#include <vector>

#include "TObject.h"
#include "TObjArray.h"
#include "TBranch.h"

#include "AbsRunInfo.hh"
#include "AbsEventInfo.hh"

class AbsData;
class AbsNode;

class AbsEvent : public TObject {
private:
  AbsEvent();
  virtual ~AbsEvent();

public:
  static AbsEvent * Instance();
  static void DeleteInstance();

  int AddData(const char * branchName, AbsData * data);
  int AddInputData(TBranch * branch, AbsNode *& node);
  int AddDropInputData(const char * branchName, const char * prodName = "");
  int AddDropOutputData(const char * branchName, const char * prodName = "");

  void SetRunInfo(AbsRunInfo * run);

  void SetRunNumber(int num);
  void SetEventNumber(int num);
  void SetTriggerNumber(int num);
  void SetEntryNumber(int num);

  AbsData * GetData(const char * name, const char * prodName = NULL);
  TObjArray * GetOutputData();

  int GetRunNumber() const;
  int GetEventNumber() const;
  int GetTriggerNumber() const;
  int GetEntryNumber() const;

  AbsRunInfo * GetRunInfo() const;
  AbsEventInfo * GetEventInfo() const;

  void AddObject(TObject * object);
  TObject * GetObject(const char * name) const;

private:
  static AbsEvent * sfTheEvent;

  int fRunNumber;
  int fEventNumber;
  int fTriggerNumber;
  int fEntryNumber;

  //Node Lists
  TObjArray * fListOfNodes;
  TObjArray * fListOfInputNodes;
  TObjArray * fListOfOutputNodes;
  TObjArray * fDropInputList;
  TObjArray * fDropOutputList;
  TObjArray * fListOfObject;

  AbsNode * fEventInfo;

  AbsRunInfo * fRunInfo;

ClassDef(AbsEvent, 0)
};

inline void AbsEvent::SetRunInfo(AbsRunInfo * run)
{
  fRunInfo = run;
}

inline AbsRunInfo * AbsEvent::GetRunInfo() const
{
  return fRunInfo;
}

inline void AbsEvent::SetRunNumber(int num)
{
  fRunNumber = num;
}

inline int AbsEvent::GetRunNumber() const
{
  return fRunNumber;
}

inline void AbsEvent::SetEventNumber(int num)
{
  fEventNumber = num;
}

inline int AbsEvent::GetEventNumber() const
{
  return fEventNumber;
}

inline void AbsEvent::SetTriggerNumber(int num)
{
  fTriggerNumber = num;
}

inline int AbsEvent::GetTriggerNumber() const
{
  return fTriggerNumber;
}

inline void AbsEvent::SetEntryNumber(int num)
{
  fEntryNumber = num;
}

inline int AbsEvent::GetEntryNumber() const
{
  return fEntryNumber;
}

inline void AbsEvent::AddObject(TObject * object)
{
  fListOfObject->Add(object);
}

inline TObject * AbsEvent::GetObject(const char * name) const
{
  return fListOfObject->FindObject(name);
}

#endif

/**
$Log: AbsEvent.hh,v $
Revision 1.13  2018/11/20 07:54:48  cupsoft
*** empty log message ***

Revision 1.12  2015/08/07 07:55:09  jslee
*** empty log message ***

Revision 1.11  2015/08/07 04:51:10  jslee
*** empty log message ***

Revision 1.10  2015/07/25 10:43:37  daq
update

Revision 1.9  2015/06/25 03:33:27  jslee
*** empty log message ***

Revision 1.8  2015/06/06 07:23:06  jslee
*** empty log message ***

Revision 1.7  2015/06/06 06:15:12  jslee
rearrange run & event numbering

Revision 1.6  2015/04/24 09:11:21  jslee
delete ValueColl in AbsEvent

Revision 1.5  2015/03/19 04:53:06  jslee
delete static member for signleton

**/

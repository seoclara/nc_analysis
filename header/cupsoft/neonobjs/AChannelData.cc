/*
 *
 *  Module:  AChannelData/RawObjs
 *
 *  Author:  Jaison Lee
 *
 *  Purpose: TClonesArray for AChannels
 *
 *  Last Update:      $Author: cupsoft $
 *  Update Date:      $Date: 2016/07/14 07:58:51 $
 *  CVS/RCS Revision: $Revision: 1.1.1.1 $
 *  Status:           $State: Exp $
 *
 */

#include "AChannel.hh"
#include "AChannelData.hh"

ClassImp(AChannelData)

AChannelData::AChannelData()
:AbsData("AChannelData")
{
  fNCh  = 0;
  fColl = new AChannelColl("AChannel", 100);
}

AChannelData::AChannelData(const AChannelData & data)
  :AbsData(data),
   fColl(data.GetColl())
{
}

AChannelData::~AChannelData()
{
  delete fColl;  
}

AChannel * AChannelData::Add()
{
  AChannel * aAChannel;
  
  aAChannel = new ((*fColl)[fNCh++]) AChannel();
  
  return aAChannel;
}

AChannel * AChannelData::Add(Int_t id)
{
  AChannel * aAChannel;
  
  aAChannel = new ((*fColl)[fNCh++]) AChannel(id);
  
  return aAChannel;
}

AChannel * AChannelData::Add(AChannel * ch)
{
  AChannel * aAChannel;
  
  aAChannel = new ((*fColl)[fNCh++]) AChannel(*ch);
  
  return aAChannel;
}

AChannel * AChannelData::GetChannel(Int_t id) const
{
  AChannel * aAChannel = 0;

  int n = GetN();
  for(int i = 0; i < n; i++){
    AChannel * rch = Get(i);
    if(rch->GetID() == id){
      aAChannel = rch;
      break;
    }
  }

  return aAChannel;
}

void AChannelData::CopyFrom(const AChannelData * data)
{
  int nch = data->GetN();

  for(int i = 0; i < nch; i++){
    AChannel * ch = data->Get(i);
    this->Add(ch);
  }
}

void AChannelData::Clear(const Option_t *)
{
  fColl->Delete();
  fNCh = 0;
}

void AChannelData::Dump() const
{

}

/**
$Log: AChannelData.cc,v $
Revision 1.1.1.1  2016/07/14 07:58:51  cupsoft
RawObjs

Revision 1.1.1.1  2016/02/29 08:25:13  cupsoft
RawObjs

**/

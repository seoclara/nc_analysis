//--------------------------------------------------------------------------
// File :
//      RACInputModule.hh
//
// Description:
//      Class RACInputModule is the abstract class for managubg root input
//
// Author List:
//      Jaison Lee                Original Author
//
// Jan-30-2007 (Jaison Lee) : RACInputModule start
// Nov-25-2013 (Jaison Lee) : modified for general framework
//
// Last Update:      $Author: jslee $
// Update Date:      $Date: 2015/03/23 03:57:51 $
// CVS/RCS Revision: $Revision: 1.5 $
// Status:           $State: Exp $
//------------------------------------------------------------------------

#ifndef RACInputModule_HH
#define RACInputModule_HH

#include "TFile.h"
#include "TChain.h"

#include "AbsEvent.hh"
#include "RACModule.hh"

class RACInputModule : public RACModule {
public:
  RACInputModule(const char * name  = "RACInputModule", 
		 const char * title = "RACInputModule" );
  virtual ~RACInputModule();

  void InitChain(const char * treeName);
  int  AddFile  (const char * fileName);

  int  GetEntries();
  virtual int GetEntry(int i) = 0;

  void SetNReport(int n);
  void PrintReport();

protected:
  bool     fOwnChain;

  TFile  * fFile ;
  TChain * fChain;

  Long64_t fNBytes;
  Long64_t fTotalProcessedEntry;

  int fNReport;

  ClassDef(RACInputModule, 0)
};

inline void RACInputModule::SetNReport(int n) 
{
  fNReport = n; 
}

#endif

/**
$Log: RACInputModule.hh,v $
Revision 1.5  2015/03/23 03:57:51  jslee
bug fixed, read tree size

**/

//--------------------------------------------------------------------------
// File :
//      RACRootManager.hh
//
// Description:
//      Class RACRootManager is a class for ROOT file input & output
//
// Author List:
//      Jaison Lee                Original Author
//
// Jan-30-2007 (Jaison Lee) : RACRootManager start
// Nov-25-2013 (Jaison Lee) : modified for general framework
//
//------------------------------------------------------------------------

#ifndef RACRootManager_HH
#define RACRootManager_HH

//------------------------------------------------------------------------
// C and C++ header
//------------------------------------------------------------------------

//------------------------------------------------------------------------
// Utilities header
//------------------------------------------------------------------------
#include "TFile.h"
#include "TObject.h"
#include "TObjArray.h"
#include "TString.h"
#include "TSocket.h"

//------------------------------------------------------------------------
// Collaboration header
//------------------------------------------------------------------------

class RACRootManager : public TObject {
private:
  RACRootManager();
  ~RACRootManager();

public:
  static RACRootManager * Instance();
  static void DeleteInstance();

  bool OpenFile();
  bool CloseFile();

  void SetMemFile();
  void SetFileName(const char * fname);
  void SetDirectory(const char * dname);
  void Add(TObject * obj);

  bool IsMemFile() const;

  TFile * GetFile();
  const char * GetFileName() const;

  TObject * Find(const char * dirname, const char * name);
  void SendTo(TSocket * socket);

  void WriteToFile(int option = 0);
  void Update();



private:
  static RACRootManager * fTheRootManager;

  TFile     * fRootFile  ;
  TObjArray * fObjectList;
  TString     fDirName   ;
  TString     fFileName  ;

  bool fMemFile;

  ClassDef(RACRootManager, 0)
};

inline void RACRootManager::SetMemFile()
{
  fMemFile = kTRUE;
}

inline bool RACRootManager::IsMemFile() const
{
  return fMemFile ? kTRUE : kFALSE;
}

inline void RACRootManager::SetFileName(const char * fname)
{
  fFileName = fname;
}

inline const char * RACRootManager::GetFileName() const
{
  return fFileName.Data();
}

inline TFile * RACRootManager::GetFile()
{
  return fRootFile;
}
#endif

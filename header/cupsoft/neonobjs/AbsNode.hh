//--------------------------------------------------------------------------
// File :
//      AbsNode.hh
//
// Description:
//      Class AbsNode is the class for connecting AbsEvent with AbsData
//      using ROOT tree structure
//
// Author List:
//      Jaison Lee                Original Author
//
// Jan-30-2007 (Jaison Lee) : AbsNode start
// Nov-25-2013 (Jaison Lee) : modified for general framework
//
//------------------------------------------------------------------------

#ifndef AbsNode_HH
#define AbsNode_HH

#include "TNamed.h"
#include "TBranch.h"

#include "AbsEvent.hh"
#include "AbsData.hh"

class AbsNode : public TNamed {
public:
  AbsNode();
  AbsNode(const char * branchName,
	  TClass *     cl,
	  AbsEvent *   event);
  AbsNode(TBranch *    branch,
	  TClass *     cl,
	  AbsEvent *   event);
  AbsNode(const char * branchName,
	  AbsData *    data,
	  AbsEvent *   event);


  virtual ~AbsNode();

  void SetBranch(TBranch * b);

  AbsData  *  GetData();
  AbsData  ** GetDataAddress();
  TBranch  *  GetBranch();
  TBranch  ** GetBranchAddress();
  AbsEvent *  GetEvent();
  const char * GetDataProdName() const;

  int GetSplitLevel() const;
  void SetSplitLevel(int n);

  virtual int GetEntry(int ientry);

protected:
  TBranch  * fBranch;
  AbsEvent * fEvent ;
  AbsData  * fObject;

  int        fSplitLevel;
  int        fDeleteObject; //!
  
  ClassDef(AbsNode, 0)
};

inline AbsData * AbsNode::GetData()
{ 
  return  fObject; 
}

inline const char * AbsNode::GetDataProdName() const
{ 
  return  fObject->GetProdName(); 
}

inline AbsData ** AbsNode::GetDataAddress()   
{
  return &fObject; 
}

inline TBranch * AbsNode::GetBranch()        
{
  return  fBranch; 
}

inline TBranch ** AbsNode::GetBranchAddress() 
{
  return &fBranch; 
}

inline AbsEvent * AbsNode::GetEvent()
{
  return  fEvent ; 
}

inline void AbsNode::SetSplitLevel(int n)
{
  fSplitLevel = n; 
}

inline int AbsNode::GetSplitLevel() const
{
  return  fSplitLevel; 
}

inline void AbsNode::SetBranch(TBranch * b) 
{
  fBranch = b; 
}

#endif

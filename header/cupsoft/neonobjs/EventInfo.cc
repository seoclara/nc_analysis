#include "EventInfo.hh"

ClassImp(EventInfo)

EventInfo::EventInfo()
  : AbsEventInfo()
{
  fNADCHeader = 0;
  fADCHeaders = new TClonesArray("ADCEventHeader");
}

EventInfo::EventInfo(const EventInfo & info)
  : AbsEventInfo(info),
    fADCHeaders(info.GetADCHeaders())
{
}

EventInfo::~EventInfo()
{
  delete fADCHeaders;
}

void EventInfo::CopyFrom(const EventInfo * info)
{
  fRunNumber = info->GetRunNumber();
  fEventNumber = info->GetEventNumber();
  fTriggerNumber = info->GetTriggerNumber();
  fTriggerType = info->GetTriggerType();
  fTriggerTime = info->GetTriggerTime();

  TClonesArray * headers = info->GetADCHeaders();

  int n = headers->GetEntries();
  for (int i = 0; i < n; i++) {
    auto * header = (ADCEventHeader *)headers->At(i);
    AddADCHeader(header);
  }
}

void EventInfo::AddADCHeader(ADCHeader * header)
{
  new((*fADCHeaders)[fNADCHeader++]) ADCEventHeader(*header);
}

void EventInfo::AddADCHeader(ADCEventHeader * header)
{
  new((*fADCHeaders)[fNADCHeader++]) ADCEventHeader(*header);
}

ADCEventHeader * EventInfo::GetADCHeader(int mid) const
{
  ADCEventHeader * h = nullptr;
  int n = fADCHeaders->GetEntries();

  for (int i = 0; i < n; i++) {
    auto * header = (ADCEventHeader *)fADCHeaders->At(i);
    if (header->GetMID() == mid) {
      h = header;
      break;
    }
  }

  if (!h) {
    Warning("GetADCHeader", "no header [mid=%d]", mid);
  }

  return h;
}

unsigned int EventInfo::GetLocalTriggerNumber(int mid) const
{
  ADCEventHeader * h = GetADCHeader(mid);

  if (!h) {
    return 0;
  }

  return h->GetTriggerNumber();
}

unsigned long EventInfo::GetLocalTriggerTime(int mid) const
{
  ADCEventHeader * h = GetADCHeader(mid);

  if (!h) {
    return 0;
  }

  return h->GetTriggerTime();
}

void EventInfo::Clear(const Option_t * opt)
{
  fADCHeaders->Delete();
  fNADCHeader = 0;
}

void EventInfo::Dump() const
{
  Info("Dump", "NTRG=%10d  TTRG=%16ld TTYPE=%d", fTriggerNumber, fTriggerTime, fTriggerType);
}

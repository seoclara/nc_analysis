/*
 *
 *  Module:  FChannelIData/RawObjs
 *
 *  Author:  Jaison Lee
 *
 *  Purpose: TClonesArray for FChannelIs
 *
 *  Last Update:      $Author: cupsoft $
 *  Update Date:      $Date: 2020/01/14 02:26:08 $
 *  CVS/RCS Revision: $Revision: 1.3 $
 *  Status:           $State: Exp $
 *
 */

#include "FChannelI.hh"
#include "FChannelIData.hh"

ClassImp(FChannelIData)

FChannelIData::FChannelIData()
  : AbsData("FChannelIData")
{
  fNCh = 0;
  fColl = new FChannelColl("FChannelI", 100);
}

FChannelIData::FChannelIData(const FChannelIData & data)
  : AbsData(data),
    fColl(data.GetColl())
{
}

FChannelIData::~FChannelIData()
{
  delete fColl;
}

FChannelI * FChannelIData::Add()
{
  FChannelI * aFChannelI;

  aFChannelI = new((*fColl)[fNCh++]) FChannelI();

  return aFChannelI;
}

FChannelI * FChannelIData::Add(Int_t id, Int_t ndp)
{
  FChannelI * aFChannelI;

  aFChannelI = new((*fColl)[fNCh++]) FChannelI(id, ndp);

  return aFChannelI;
}

FChannelI * FChannelIData::Add(Int_t id, Int_t ndp, const unsigned int * wave)
{
  FChannelI * aFChannelI;

  aFChannelI = new((*fColl)[fNCh++]) FChannelI(id, ndp, wave);

  return aFChannelI;
}

FChannelI * FChannelIData::Add(FChannelI * ch)
{
  FChannelI * aFChannelI;

  aFChannelI = new((*fColl)[fNCh++]) FChannelI(*ch);

  return aFChannelI;
}

FChannelI * FChannelIData::GetChannel(Int_t id) const
{
  FChannelI * aFChannelI = 0;

  int n = GetN();
  for (int i = 0; i < n; i++) {
    FChannelI * rch = Get(i);
    if (rch->GetID() == id) {
      aFChannelI = rch;
      break;
    }
  }

  return aFChannelI;
}

void FChannelIData::Clear(const Option_t *)
{
  fColl->Delete();
  fNCh = 0;
}

void FChannelIData::Dump() const
{

}

/**
$Log: FChannelIData.cc,v $
Revision 1.3  2020/01/14 02:26:08  cupsoft
*** empty log message ***

Revision 1.2  2020/01/09 07:10:55  cupsoft
*** empty log message ***

Revision 1.1.1.1  2016/07/14 07:58:51  cupsoft
RawObjs

Revision 1.2  2016/03/17 08:12:41  jslee
add inheritance from TArray class
remove member array for waveform

Revision 1.1.1.1  2016/02/29 08:25:13  cupsoft
RawObjs

**/

#include "AbsData.hh"

ClassImp(AbsData)

AbsData::AbsData()
:TNamed("","")
{
}

AbsData::AbsData(const char * name, const char * prodname)
 :TNamed(name, prodname)
{
}

AbsData::AbsData(const AbsData & data)
  :TNamed(data)
{
}

AbsData::~AbsData()
{
}

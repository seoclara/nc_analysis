#include "AbsEventInfo.hh"

ClassImp(AbsEventInfo)

AbsEventInfo::AbsEventInfo()
  : AbsData()
{
  fRunNumber = 0;
  fEventNumber = 0;
  fTriggerType = 0;
  fTriggerNumber = 0;
  fTriggerTime = 0;
}

AbsEventInfo::AbsEventInfo(const AbsEventInfo & evt)
  : AbsData(evt)
{
  fRunNumber = evt.GetRunNumber();
  fEventNumber = evt.GetEventNumber();
  fTriggerType = evt.GetTriggerType();
  fTriggerNumber = evt.GetTriggerNumber();
  fTriggerTime = evt.GetTriggerTime();
}

AbsEventInfo::~AbsEventInfo()
{
}

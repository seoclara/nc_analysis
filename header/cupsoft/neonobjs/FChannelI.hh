/*
 *
 *  Module:  FChannelI/RawObjs
 *
 *  Author:  Jaison Lee
 *
 *  Purpose: A Class for storing digitzed waveform
 *
 *  Last Update:      $Author: cupsoft $
 *  Update Date:      $Date: 2020/01/14 02:26:08 $
 *  CVS/RCS Revision: $Revision: 1.3 $
 *  Status:           $State: Exp $
 *
 */

#ifndef FChannelI_hh
#define FChannelI_hh

#include <vector>

#include "ArrayI.hh"
#include "AbsChannel.hh"

class TH1D;
class FChannelI : public AbsChannel, public ArrayI 
{
public:
  FChannelI();
  FChannelI(Int_t id);
  FChannelI(Int_t id, Int_t ndp);
  FChannelI(Int_t id, Int_t ndp, const unsigned int * wave);
  FChannelI(const FChannelI & ch);

  virtual ~FChannelI();
  
  void SetNdp(Int_t ndp);
  void SetWaveform(const unsigned int * wave);
  void SetWaveform(Int_t ndp, const unsigned int * wave);
  void SetWaveform(Int_t n, unsigned int count);
  void SetPedestal(Int_t ped);

  Int_t GetNdp() const;
  unsigned int GetPedestal() const;
  const unsigned int * GetWaveform() const;

  TH1D * GetWaveformHist(double pedm = 0);

protected:
  unsigned int fPedestal;// pedestal from FADC
  TH1D * fWaveHis ;//! just for drawing waveform
  TH1D * fWaveHisC ;//! just for drawing waveform Converted NI Data
  
  ClassDef(FChannelI, 1)
};

//
// Inline functions
//
inline void FChannelI::SetPedestal(Int_t ped)
{
  fPedestal = ped;
}

inline unsigned int FChannelI::GetPedestal() const
{ 
  return fPedestal;
}

#endif

/**
$Log: FChannelI.hh,v $
Revision 1.3  2020/01/14 02:26:08  cupsoft
*** empty log message ***

Revision 1.2  2017/03/20 05:41:55  ysy
.

Revision 1.1.1.1  2016/07/14 07:58:51  cupsoft
RawObjs

Revision 1.3  2016/03/17 08:12:12  jslee
add inheritance from TArray class
remove member array for waveform

Revision 1.2  2016/03/08 04:40:33  amore
*** empty log message ***

Revision 1.1.1.1  2016/02/29 08:25:13  cupsoft
RawObjs

**/

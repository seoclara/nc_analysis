// @(#)root/cont:$Id: ArrayI.hh,v 1.1 2020/01/14 02:26:08 cupsoft Exp $
// Author: Rene Brun   06/03/95

/*************************************************************************
 * Copyright (C) 1995-2000, Rene Brun and Fons Rademakers.               *
 * All rights reserved.                                                  *
 *                                                                       *
 * For the licensing terms see $ROOTSYS/LICENSE.                         *
 * For the list of contributors see $ROOTSYS/README/CREDITS.             *
 *************************************************************************/

#ifndef ROOT_ArrayI
#define ROOT_ArrayI


//////////////////////////////////////////////////////////////////////////
//                                                                      //
// ArrayI                                                              //
//                                                                      //
// Array of integers (32 bits per element).                             //
//                                                                      //
//////////////////////////////////////////////////////////////////////////

#include "TArray.h"

class ArrayI : public TArray {

public:
  unsigned int * fArray;       //[fN] Array of fN 32 bit integers

  ArrayI();
  ArrayI(Int_t n);
  ArrayI(Int_t n, const unsigned int * array);
  ArrayI(const ArrayI & array);
  ArrayI & operator=(const ArrayI & rhs);
  virtual    ~ArrayI();

  void Adopt(Int_t n, unsigned int * array);
  void AddAt(unsigned int c, Int_t i);
  unsigned int At(Int_t i) const;
  void Copy(ArrayI & array) const
  { array.Set(fN, fArray); }
  const unsigned int * GetArray() const
  { return fArray; }
  unsigned int * GetArray()
  { return fArray; }
  Double_t GetAt(Int_t i) const
  { return At(i); }
  Stat_t GetSum() const
  {
    Stat_t sum = 0;
    for (Int_t i = 0; i < fN; i++) sum += fArray[i];
    return sum;
  }
  void Reset()
  { memset(fArray, 0, fN*sizeof(unsigned int)); }
  void Reset(unsigned int val)
  { for (Int_t i = 0; i < fN; i++) fArray[i] = val; }
  void Set(Int_t n);
  void Set(Int_t n, const unsigned int * array);
  void SetAt(Double_t v, Int_t i)
  { AddAt((unsigned int)v, i); }
  unsigned int & operator[](Int_t i);
  unsigned int operator[](Int_t i) const;

ClassDef(ArrayI, 1)  //Array of ints
};

#if defined R__TEMPLATE_OVERLOAD_BUG
template <>
#endif
inline TBuffer & operator>>(TBuffer & buf, ArrayI *& obj)
{
  // Read ArrayI object from buffer.

  obj = (ArrayI *)TArray::ReadArray(buf, ArrayI::Class());
  return buf;
}

#if defined R__TEMPLATE_OVERLOAD_BUG
template <>
#endif
inline TBuffer & operator<<(TBuffer & buf, const ArrayI * obj)
{
  // Write a ArrayI object into buffer
  return buf << (const TArray *)obj;
}

inline unsigned int ArrayI::At(Int_t i) const
{
  if (!BoundsOk("ArrayI::At", i)) return 0;
  return fArray[i];
}

inline unsigned int & ArrayI::operator[](Int_t i)
{
  if (!BoundsOk("ArrayI::operator[]", i)) {
     i = 0;
  }
  return fArray[i];
}

inline unsigned int ArrayI::operator[](Int_t i) const
{
  if (!BoundsOk("ArrayI::operator[]", i)) return 0;
  return fArray[i];
}

#endif

/*
 *
 *  Module:  FChannelS/RawObjs
 *
 *  Author:  Jaison Lee
 *
 *  Purpose: A Class for storing FADC digitzed waveform
 *
 *  Last Update:      $Author: cupsoft $
 *  Update Date:      $Date: 2020/01/14 02:26:08 $
 *  CVS/RCS Revision: $Revision: 1.2 $
 *  Status:           $State: Exp $
 *
 */

#include <iostream>

#include "TMath.h"
#include "TH1D.h"

#include "FChannelS.hh"

ClassImp(FChannelS)

FChannelS::FChannelS()
: AbsChannel(), ArrayS()
{
  fWaveHis  = NULL;
  fPedestal = 0;
}

FChannelS::FChannelS(Int_t id, Int_t ndp)
  : AbsChannel(id), ArrayS(ndp)
{
  fWaveHis = NULL;
  fPedestal = 0;
}

FChannelS::FChannelS(Int_t id, Int_t ndp, const unsigned short * wave)
  : AbsChannel(id), ArrayS(ndp, wave)
{
  fWaveHis = NULL;
  fPedestal = 0;
}

FChannelS::FChannelS(const FChannelS & ch)
  : AbsChannel(ch), ArrayS(ch)
{
  fWaveHis = NULL;
  fPedestal = ch.GetPedestal();
}

FChannelS::~FChannelS()
{
  if(fWaveHis != NULL) delete fWaveHis;
}

void FChannelS::SetNdp(Int_t ndp)
{
  Set(ndp);
}

void FChannelS::SetWaveform(const unsigned short * wave)
{
  if(GetSize() == 0){
    Error("SetWaveform", "Number of data point (ndp) is not set");
    return;
  }
  
  Int_t n = GetSize();
  Set(n, wave);
}

void FChannelS::SetWaveform(Int_t ndp, const unsigned short * wave)
{
  Set(ndp, wave);
}


void FChannelS::SetWaveform(Int_t n, unsigned short count)
{
  if(GetSize() == 0){
    Error("SetWaveform", "Number of data point (ndp) is not set");
    return;
  }

  AddAt(count, n);
}

Int_t FChannelS::GetNdp() const      
{ 
  return GetSize();
}

const unsigned short * FChannelS::GetWaveform() const
{ 
  return GetArray(); 
}

TH1D * FChannelS::GetWaveformHist(Double_t pedm)
{
  int ndp = GetSize();

  if(!fWaveHis){
    fWaveHis = new TH1D(Form("Waveform_ch%03d", fID),"", ndp, 0, ndp);
  }

  if(pedm == 0){
    pedm = fPedestal;
  }

  fWaveHis->Reset();
  for(Int_t i = 1; i <= ndp; i++){
    fWaveHis->SetBinContent(i, At(i-1)-pedm);
  }

  return fWaveHis;
}

/**
$Log: FChannelS.cc,v $
Revision 1.2  2020/01/14 02:26:08  cupsoft
*** empty log message ***

Revision 1.1.1.1  2016/07/14 07:58:51  cupsoft
RawObjs

Revision 1.3  2016/03/17 08:12:41  jslee
add inheritance from TArray class
remove member array for waveform

Revision 1.2  2016/03/08 04:40:33  amore
*** empty log message ***

Revision 1.1.1.1  2016/02/29 08:25:13  cupsoft
RawObjs

**/

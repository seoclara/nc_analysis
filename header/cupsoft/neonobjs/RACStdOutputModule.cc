#include <iostream>

#include "TSystem.h"
#include "TObjArray.h"
#include "TObjString.h"
#include "TBranchRef.h"

#include "AbsNode.hh"
#include "RACStdOutputModule.hh"

using namespace std;

ClassImp(RACStdOutputModule)

RACStdOutputModule::RACStdOutputModule(const char * name, const char * title)
  :RACModule(name, title)
{
  SetType(RACModule::OUTPUT);

  fNFileNumber         = 0    ;
  fMaxFileSize         = 2000 ;
  fTotalProcessedEntry = 0    ;
  fDoMakeOutputFile    = false;

  fOFile = NULL;
}

RACStdOutputModule::~RACStdOutputModule()
{
}

bool RACStdOutputModule::BeginJob(AbsEvent * anEvent)
{
  bool rc = true;

  Info("BeginJob", Form("Maximum output file size: %d", fMaxFileSize));

  if(fDoMakeOutputFile){
    fFileList = new TObjArray(10);

    rc = OpenNewFile(anEvent, 0);
  }

  return rc;
}

bool RACStdOutputModule::EndJob(AbsEvent * anEvent)
{
  Long64_t mbytesWritten = 0;
  if(fDoMakeOutputFile){
    if(fOFile){
      mbytesWritten = fOFile->GetFileBytesWritten();
      fOFile->cd();
      fOTree->Write();
      fOFile->Close();
      delete fOFile;
      fOFile = NULL;
    }

    cout<<endl;
    cout<<"******************************************************************"<<endl;
    cout<<"   Standard Output Module Report                                  "<<endl;
    cout<<"   =============================                                  "<<endl;
    cout<<endl;
    cout<<"   Output Files :"<<endl;

    TObjString * name;
    TIter it(fFileList);
    while((name = (TObjString*)it.Next())){
      if(name){
	cout<<"      "<<name->GetName()<<endl;
	delete name;
      }
    }
    delete fFileList;

    cout<<"   Total write [MBytes]    : "<<mbytesWritten/1024./1024.   <<endl;
    cout<<"   Total processed Entries : "<<fTotalProcessedEntry<<endl;
    cout<<"******************************************************************"<<endl;
    cout<<endl;
  }

  return true;
}

bool RACStdOutputModule::Event(AbsEvent * anEvent)
{
  bool rc = true;

  if(fDoMakeOutputFile){
    Long64_t mbytesWritten = fOFile->GetBytesWritten();
    if((int)(mbytesWritten/1024./1024.) >= fMaxFileSize){

      fNFileNumber += 1;
      rc = OpenNewFile(anEvent, fNFileNumber);
    }

    fOFile->cd();
    fOTree->Fill();
    
    fTotalProcessedEntry += 1;
  }

  return rc;
}

bool RACStdOutputModule::OpenNewFile(AbsEvent * anEvent, int n)
{
  if(n == 0){
    fOFile = new TFile(fFileName.Data(), "RECREATE");

    if(fOFile == 0){
      Error("OpenNewFile",Form("Can\'t open output file %s", fFileName.Data()));
      return false;
    }
    fOFile->cd();
    fOTree = new TTree("AbsEvent", "AbsEvent");
    
    TObjArray * list = anEvent->GetOutputData();
    TIter it(list);
    
    TBranch    * branch;
    AbsNode    * node  ;
    const char * branchName;
    
    while((node = (AbsNode*)it.Next())){
      branchName = node->GetName();
      branch = fOTree->Branch(branchName, branchName, 
			      node->GetDataAddress(), 32000, 
			      node->GetSplitLevel());

      branch->SetAutoDelete(kFALSE);
    }
    Info("OpenNewFile",Form("Output file %s is opened", fFileName.Data()));
    
    TObjString * filename = new TObjString(fFileName.Data());
    fFileList->Add(filename);
    
    return true;
  }
  else{
    fOFile->cd();
    fOTree->Write();
    fOTree->Reset();
    
    const char * fname = Form("%s.%03i",fFileName.Data(), n);

    int compress = fOFile->GetCompressionSettings();
    
    TFile * newfile = new TFile(fname, "recreate", "chain files", compress);
    if(newfile == 0){
      Error("OpenNewFile",Form("Can\'t open output file %s", fname));
      return false;
    }
    else
      Info("OpenNewFile",Form("Output file %s is opened", fname));
    
    TBranch * branch = 0;
    TObject * obj    = 0;
    
    while((obj = fOFile->GetList()->First())){
      fOFile->Remove(obj);
      
      if(obj->InheritsFrom(TTree::Class())){
	TTree * t = (TTree*)obj;
	t->SetDirectory(newfile);
	
	TIter nextb(t->GetListOfBranches());
	while((branch = (TBranch*)nextb())){
	  branch->SetFile(newfile);
	}
	
	if(t->GetBranchRef()){
	  t->GetBranchRef()->SetFile(newfile);
	}
	continue;
      }
      
      if(newfile) newfile->Append(obj);
      fOFile->Remove(obj);
    }
    
    delete fOFile;
    fOFile = NULL;

    fOFile = newfile;

    if(n == 1) {
      gSystem->Exec(Form("mv %s %s.000", fFileName.Data(), fFileName.Data()));
    }

    TObjString * filename = new TObjString(fname);
    fFileList->Add(filename);
    
    return true;
  }

  return true;
}

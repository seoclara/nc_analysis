//--------------------------------------------------------------------------
// File :
//      RACStdInputModule.hh
//
// Description:
//      Class RACStdInputModule is the class for processing framework
//      standard input file
//
// Author List:
//      Jaison Lee                Original Author
//
// Jan-30-2007 (Jaison Lee) : RACStdInputModule start
// Nov-25-2013 (Jaison Lee) : modified for general framework
//
// Last Update:      $Author: jslee $
// Update Date:      $Date: 2015/03/23 03:57:51 $
// CVS/RCS Revision: $Revision: 1.5 $
// Status:           $State: Exp $
//------------------------------------------------------------------------

#ifndef RACStdInputModule_HH
#define RACStdInputModule_HH

#include "AbsEvent.hh"
#include "AbsEventInfo.hh"
#include "RACInputModule.hh"

class RACStdInputModule : public RACInputModule {
public:
  RACStdInputModule(const char * name  = "RACStdInputModule",
		    const char * title = "RACStdInputModule");
  virtual ~RACStdInputModule();

  virtual bool BeginJob (AbsEvent * anEvent);
  virtual bool EndJob   (AbsEvent * anEvent);
  virtual bool BeginRun (AbsEvent * anEvent){ return true; }
  virtual bool EndRun   (AbsEvent * anEvent){ return true; }
  virtual bool Event    (AbsEvent * anEvent){ return true; }

  virtual int GetEntry(int i);

protected:
  AbsEventInfo * fEventInfo;


  ClassDef(RACStdInputModule, 0)
};

#endif

/**
$Log: RACStdInputModule.hh,v $
Revision 1.5  2015/03/23 03:57:51  jslee
bug fixed, read tree size

**/

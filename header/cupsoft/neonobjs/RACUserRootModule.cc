//------------------------------------------------------------------------
// C and C++ header
//------------------------------------------------------------------------
#include <iostream>
//------------------------------------------------------------------------
// Utilities header
//------------------------------------------------------------------------

//------------------------------------------------------------------------
// Collaboration header
//------------------------------------------------------------------------
#include "RACUserRootModule.hh"
#include "RACRootManager.hh"

using namespace std;

ClassImp(RACUserRootModule)

RACUserRootModule::RACUserRootModule(const char * name, const char * title)
: RACRootModule(name, title)
{
}

RACUserRootModule::~RACUserRootModule()
{
}

bool RACUserRootModule::BeginJob(AbsEvent * anEvent)
{
  fRootManager->SetFileName(fRootFileName);
  if(fRootManager->OpenFile()) return true;
  return false;
}

bool RACUserRootModule::EndJob(AbsEvent * anEvent)
{
  if(fRootManager->CloseFile()) return true;
  return false;
}

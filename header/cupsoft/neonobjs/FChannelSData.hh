/*
 *
 *  Module:  FChannelSData/RawObjs
 *
 *  Author:  Jaison Lee
 *
 *  Purpose: TClonesArray for FChannelSs
 *
 *  Last Update:      $Author: cupsoft $
 *  Update Date:      $Date: 2020/01/14 02:26:08 $
 *  CVS/RCS Revision: $Revision: 1.2 $
 *  Status:           $State: Exp $
 *
 */

#ifndef FChannelSData_hh
#define FChannelSData_hh

#include "TClonesArray.h"

#include "AbsData.hh"
#include "FChannelS.hh"

typedef TClonesArray FChannelColl;

class FChannelS;
class FChannelSData : public AbsData 
{
public:
  FChannelSData();
  FChannelSData(const FChannelSData & data);
  virtual ~FChannelSData();
  
  virtual void Clear(const Option_t * opt = "");
  virtual void Dump() const;
  
  FChannelS * Add();
  FChannelS * Add(Int_t id, Int_t ndp);
  FChannelS * Add(Int_t id, Int_t ndp, const unsigned short * wave);
  FChannelS * Add(FChannelS * ch);

  FChannelColl * GetColl()    const;
  Int_t          GetN()       const;
  FChannelS    * Get(Int_t n) const;

  FChannelS    * GetChannel(Int_t id) const;

  void CopyFrom(const FChannelSData * data);

private:
  Int_t          fNCh ;//! just for counter
  FChannelColl * fColl;
  
  ClassDef(FChannelSData, 1)
};

//
// Inline functions
//

inline FChannelColl * FChannelSData::GetColl() const
{
  return fColl; 
}

inline Int_t FChannelSData::GetN() const
{
  return fColl->GetEntries(); 
}

inline FChannelS * FChannelSData::Get(Int_t n) const
{
  return (FChannelS*)fColl->At(n); 
}

#endif

/**
$Log: FChannelSData.hh,v $
Revision 1.2  2020/01/14 02:26:08  cupsoft
*** empty log message ***

Revision 1.1.1.1  2016/07/14 07:58:51  cupsoft
RawObjs

Revision 1.2  2016/03/17 08:12:12  jslee
add inheritance from TArray class
remove member array for waveform

Revision 1.1.1.1  2016/02/29 08:25:13  cupsoft
RawObjs

**/

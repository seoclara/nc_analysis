//--------------------------------------------------------------------------
// File :
//      RACStdOutputModule.hh
//
// Description:
//      Class RACStdOutputModule is the class for processing framework
//      standard output file
//
// Author List:
//      Jaison Lee                Original Author
//
// Jan-30-2007 (Jaison Lee) : RACStdOutputModule start
// Nov-25-2013 (Jaison Lee) : modified for general framework
//
//------------------------------------------------------------------------

#ifndef RACStdOutputModule_HH
#define RACStdOutputModule_HH

#include <iostream>

#include "TFile.h"
#include "TTree.h"
#include "TObjArray.h"

#include "AbsEvent.hh"
#include "RACModule.hh"

class RACStdOutputModule : public RACModule {
public:
  RACStdOutputModule(const char * name  = "RACStdOutputModule",
		     const char * title = "RACStdOutputModule");
  virtual ~RACStdOutputModule();

  virtual bool BeginJob (AbsEvent * anEvent);
  virtual bool EndJob   (AbsEvent * anEvent);
  virtual bool BeginRun (AbsEvent * anEvent){ return true; }
  virtual bool EndRun   (AbsEvent * anEvent){ return true; }
  virtual bool Event    (AbsEvent * anEvent);


  void SetFileName(const char * fileName) { 
    fFileName = fileName; 
    fDoMakeOutputFile = true;
  }

  //  bool OpenNewFile(AbsEvent * anEvent, const char * fname);
  bool OpenNewFile(AbsEvent * anEvent, int n);
  void SetMaxFileSize(int val) { fMaxFileSize = val; }

private:
  TFile     * fOFile              ;
  TTree     * fOTree              ;

  TString     fFileName           ;

  bool        fDoMakeOutputFile   ;

  int         fMaxFileSize        ;
  int         fNFileNumber        ;
  TObjArray * fFileList           ;

  int         fTotalProcessedEntry;

  ClassDef(RACStdOutputModule, 0)
};

#endif

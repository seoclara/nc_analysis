//------------------------------------------------------------------------
// C and C++ header
//------------------------------------------------------------------------

//------------------------------------------------------------------------
// Utilities header
//------------------------------------------------------------------------
#include "TString.h"
#include "TObjString.h"
#include "TMap.h"
#include "TMemFile.h"
#include "TMessage.h"

//------------------------------------------------------------------------
// Collaboration header
//------------------------------------------------------------------------
#include "RACRootManager.hh"

RACRootManager * RACRootManager::fTheRootManager;

RACRootManager::RACRootManager()
  :TObject()
{
  fMemFile = kFALSE;
  fRootFile = 0;
  fObjectList = new TObjArray();
}

RACRootManager::~RACRootManager()
{
  fObjectList->Clear("C");
  delete fObjectList;

  if(fRootFile) delete fRootFile;
  fRootFile = 0;
}

RACRootManager * RACRootManager::Instance()
{
  if(fTheRootManager == 0) fTheRootManager = new RACRootManager();
  return fTheRootManager;
}

void RACRootManager::DeleteInstance()
{
  if(fTheRootManager != NULL)
    delete fTheRootManager;

  fTheRootManager = NULL;
}

bool RACRootManager::OpenFile()
{
  if(!fFileName.Contains("root"))
    fFileName = "hists.root";

  if(!fMemFile)
    fRootFile = TFile::Open(fFileName.Data(), "RECREATE", "", 0);
  else
    fRootFile = new TMemFile(fFileName.Data(), "RECREATE");

  if(!fRootFile || !fRootFile->IsOpen()){
    Error("OpenFile", "file %s open failed", fFileName.Data());
    delete fRootFile;
    fRootFile = 0;
    return false;
  }

  Info("OpenFile", "user root file %s opened", fFileName.Data());

  return true;
}

bool RACRootManager::CloseFile()
{
  if(fRootFile){
    WriteToFile();
    fRootFile->Close();

    delete fRootFile;
    fRootFile = 0;

    Info("CloseFile", "user root file %s closed", fFileName.Data());

    return true;
  }

  Error("CloseFile", "user root file %s close falied", fFileName.Data());

  return false;
}

void RACRootManager::SetDirectory(const char * dname)
{
  fDirName = dname;

  if(fRootFile){
    fRootFile->cd();
    TDirectory * dir = fRootFile->mkdir(dname);
    dir->cd();
  }
}

void RACRootManager::Add(TObject * obj)
{
  TPair * pair = new TPair(new TObjString(fDirName.Data()), obj);
  fObjectList->Add(pair);
}

TObject * RACRootManager::Find(const char * dirname, const char * name)
{
  TPair * obj;
  TObject * tobj = NULL;

  TObjArrayIter iter(fObjectList);
  while((obj = (TPair*)iter.Next())){
    if(TString(obj->GetName()).EqualTo(dirname)){
      if(TString(obj->Value()->GetName()).EqualTo(name)){
        tobj = obj->Value();
      }
    }
  }

  return tobj;
}

void RACRootManager::WriteToFile(int option)
{
  TPair * obj;

  TObjArrayIter iter(fObjectList);
  while((obj = (TPair*)iter.Next())){
    fRootFile->cd(obj->GetName());
    obj->Value()->Write(0, option);
    fRootFile->cd();
  }
}

void RACRootManager::Update()
{
  WriteToFile(TObject::kOverwrite);
  fRootFile->Save();
}

void RACRootManager::SendTo(TSocket * socket)
{
  if(!fMemFile){
    Warning("SendTo", "root file is not TMemFile");
    return;
  }

  if(!socket->IsValid()){
    Warning("SendTo", "connection not established");
    return;
  }

  fRootFile->Write();

  TMessage::EnableSchemaEvolutionForAll(kFALSE);
  TMessage mess(kMESS_ANY);

  mess.WriteTString(fRootFile->GetName());
  mess.WriteLong64(fRootFile->GetEND());
  ((TMemFile*)fRootFile)->CopyTo(mess);

  int nbyte = socket->Send(mess);

  ((TMemFile*)fRootFile)->ResetAfterMerge(0);

  if(nbyte < 0)
    Error("SendTo", "socket error %d", nbyte);
  else
    Info("SendTo", "%d bytes sent to server", nbyte);
}

/*
 *
 *  Module:  FChannelSData/RawObjs
 *
 *  Author:  Jaison Lee
 *
 *  Purpose: TClonesArray for FChannelSs
 *
 *  Last Update:      $Author: cupsoft $
 *  Update Date:      $Date: 2020/01/14 02:26:08 $
 *  CVS/RCS Revision: $Revision: 1.2 $
 *  Status:           $State: Exp $
 *
 */

#include "FChannelS.hh"
#include "FChannelSData.hh"

ClassImp(FChannelSData)

FChannelSData::FChannelSData()
:AbsData("FChannelSData")
{
  fNCh  = 0;
  fColl = new FChannelColl("FChannelS", 100);
  fColl->BypassStreamer();
}

FChannelSData::FChannelSData(const FChannelSData & data)
  :AbsData(data),
   fColl(data.GetColl())
{
}

FChannelSData::~FChannelSData()
{
  delete fColl;  
}

FChannelS * FChannelSData::Add()
{
  FChannelS * aFChannelS;
  
  aFChannelS = new ((*fColl)[fNCh++]) FChannelS();
  
  return aFChannelS;
}

FChannelS * FChannelSData::Add(Int_t id, Int_t ndp)
{
  FChannelS * aFChannelS;
  
  aFChannelS = new ((*fColl)[fNCh++]) FChannelS(id, ndp);
  
  return aFChannelS;
}

FChannelS * FChannelSData::Add(Int_t id, Int_t ndp, const unsigned short * wave)
{
  FChannelS * aFChannelS;
  
  aFChannelS = new ((*fColl)[fNCh++]) FChannelS(id, ndp, wave);
  
  return aFChannelS;
}


FChannelS * FChannelSData::Add(FChannelS * ch)
{
  FChannelS * aFChannelS;
  
  aFChannelS = new ((*fColl)[fNCh++]) FChannelS(*ch);
  
  return aFChannelS;
}

FChannelS * FChannelSData::GetChannel(Int_t id) const
{
  FChannelS * aFChannelS = 0;

  int n = GetN();
  for(int i = 0; i < n; i++){
    FChannelS * rch = Get(i);
    if(rch->GetID() == id){
      aFChannelS = rch;
      break;
    }
  }

  return aFChannelS;
}

void FChannelSData::CopyFrom(const FChannelSData * data)
{
  int nch = data->GetN();

  for(int i = 0; i < nch; i++){
    FChannelS * ch = data->Get(i);
    this->Add(ch);
  }
}

void FChannelSData::Clear(const Option_t *)
{
  fColl->Delete();
  fNCh = 0;
}

void FChannelSData::Dump() const
{

}

/**
$Log: FChannelSData.cc,v $
Revision 1.2  2020/01/14 02:26:08  cupsoft
*** empty log message ***

Revision 1.1.1.1  2016/07/14 07:58:51  cupsoft
RawObjs

Revision 1.2  2016/03/17 08:12:41  jslee
add inheritance from TArray class
remove member array for waveform

Revision 1.1.1.1  2016/02/29 08:25:13  cupsoft
RawObjs

**/

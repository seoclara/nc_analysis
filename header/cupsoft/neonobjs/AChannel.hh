/*
 *
 *  Module:  AChannel/RawObjs
 *
 *  Author:  Jaison Lee
 *
 *  Purpose: A Class for storing ADC information
 *
 *  Last Update:      $Author: cupsoft $
 *  Update Date:      $Date: 2016/07/14 07:58:51 $
 *  CVS/RCS Revision: $Revision: 1.1.1.1 $
 *  Status:           $State: Exp $
 *
 */

#ifndef AChannel_hh
#define AChannel_hh

#include "AbsChannel.hh"

class AChannel : public AbsChannel
{
public:
  AChannel();
  AChannel(Int_t id);
  AChannel(const AChannel & ch);

  virtual ~AChannel();
  
  virtual void SetCount(Int_t count);
  virtual void SetTime(ULong_t time);
  
  virtual Int_t   GetCount() const;
  virtual ULong_t GetTime()  const;
  
protected:
  Int_t   fCount;// ADC count
  ULong_t fTime ;// Hit time
  
  ClassDef(AChannel, 1)
};

//
// Inline functions
//

inline void AChannel::SetCount(Int_t count)
{
  fCount = count;
}

inline void AChannel::SetTime(ULong_t time)
{
  fTime = time;
}

inline Int_t AChannel::GetCount() const      
{ 
  return fCount;
}

inline ULong_t AChannel::GetTime() const      
{ 
  return fTime;
}

#endif

/**
$Log: AChannel.hh,v $
Revision 1.1.1.1  2016/07/14 07:58:51  cupsoft
RawObjs

Revision 1.1.1.1  2016/02/29 08:25:13  cupsoft
RawObjs

**/

//------------------------------------------------------------------------
// File :
//      RACFramework.hh
//
// Description:
//      Class RACFramework is the main class for RAC
//      framework. 
//
// Author List:
//      Jaison Lee                Original Author
//
// Jan-30-2007 (Jaison Lee) : RACFramework start
// Nov-25-2013 (Jaison Lee) : modified for general framework
//
// Last Update:      $Author: cupsoft $
// Update Date:      $Date: 2018/09/21 04:08:52 $
// CVS/RCS Revision: $Revision: 1.13 $
// Status:           $State: Exp $
//------------------------------------------------------------------------

#ifndef RACFramework_HH
#define RACFramework_HH

//------------------------------------------------------------------------
// C and C++ header
//------------------------------------------------------------------------
#include <iostream>
#include <iomanip>

//------------------------------------------------------------------------
// Utilities header
//------------------------------------------------------------------------
#include "TNamed.h"
#include "TList.h"

//------------------------------------------------------------------------
// Collaboration header
//------------------------------------------------------------------------
#include "AbsEvent.hh"
#include "RACModule.hh"
#include "RACDBManager.hh"
#include "RACInputModule.hh"
#include "RACStdOutputModule.hh"
#include "RACUserRootModule.hh"

class RACFramework : public TNamed {
public:
  RACFramework();
  virtual ~RACFramework();

protected:  
  void Init();
  void UInit();

  bool BeginJob();
  bool EndJob();
  bool BeginRun();
  bool EndRun();
  bool ProcessEvent(int iEntry); 

  void SetRunNumber(int runno) { fRunNumber = runno; };
  int  GetRunNumber() const    { return fRunNumber; }

public:
  bool Run(int nev = 0, int nskip=0);

  void AddModule(RACModule * aMod);
  RACModule * AddModule(const char * clName, const char * modName = 0, const char * title = 0);

  int DropInputData(const char * dataName, const char * prodName = "");
  int DropOutputData(const char * dataName, const char * prodName = "");

  int AddStdDataInputFile(const char * fileName);

  int SetOutputFile(const char * fileName);
  int SetUserRootFile(const char * fileName, bool isMemFile = kFALSE);
  int SetDBFile(const char * fileName) { return 1; }

  void SetOutputFileMaxSize(int val);
  void SetUserRootFileUpdateRate(int nevt);

  void SetNReport(int n) { fNInputReport = n; }
  AbsEvent  * GetAbsEvent() { return fAbsEvent; }

protected:
  AbsEvent *           fAbsEvent    ;

  TList              * fModuleList;
  RACInputModule     * fInputModule ;
  RACStdOutputModule * fOutputModule;
  RACUserRootModule  * fRootManager ;
  RACDBManager       * fDBManager   ;

  bool                 fIsFirstRun  ;
  bool                 fIsFirstEvent;

  int fRunNumber                    ;
  int fNInputReport                 ;

  int fProcessedEvent;

  int fUserRootFileUpdateRate;

  ClassDef(RACFramework, 0)
};

#endif

/**
$Log: RACFramework.hh,v $
Revision 1.13  2018/09/21 04:08:52  cupsoft
*** empty log message ***

Revision 1.12  2017/09/14 07:01:46  cupsoft
*** empty log message ***

Revision 1.11  2016/07/14 07:07:02  cupsoft
*** empty log message ***

Revision 1.10  2015/03/19 05:03:21  jslee
change C++ code style

**/

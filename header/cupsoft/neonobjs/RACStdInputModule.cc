//--------------------------------------------------------------------------
// File :
//      RACStdInputModule.cc
//
// Description:
//      Class RACStdInputModule is the class for processing framework
//      standard input file
//
// Author List:
//      Jaison Lee                Original Author
//
// Jan-30-2007 (Jaison Lee) : RACStdInputModule start
// Nov-25-2013 (Jaison Lee) : modified for general framework
//
// Last Update:      $Author: jslee $
// Update Date:      $Date: 2016/02/02 09:42:00 $
// CVS/RCS Revision: $Revision: 1.16 $
// Status:           $State: Exp $
//------------------------------------------------------------------------

#include "TFile.h"
#include "TObjArray.h"

#include "AbsNode.hh"
#include "RACFramework.hh"
#include "RACStdInputModule.hh"

ClassImp(RACStdInputModule)

RACStdInputModule::RACStdInputModule(const char * name, const char * title)
  :RACInputModule(name, title)
{
  InitChain("AbsEvent");
}

RACStdInputModule::~RACStdInputModule()
{
}

bool RACStdInputModule::BeginJob(AbsEvent * anEvent)
{
  int rc;

  AbsNode * node;
  TObjArray * branchList = fChain->GetListOfBranches();
  TIter it(branchList);
  while(TBranch * branch = (TBranch*)it.Next()){
    rc = anEvent->AddInputData(branch, node);
    if(rc == 0){
      fChain->SetBranchAddress(branch->GetName(), node->GetDataAddress());
    }
    else if(rc < 0)
      return false;
  }

  fEventInfo = anEvent->GetEventInfo();

  // temporary load data
  fChain->GetEntry(0);
  // it very silly !!!
  if(fEventInfo != 0)
    anEvent->SetRunNumber(fEventInfo->GetRunNumber());
  
  return true;
}

bool RACStdInputModule::EndJob(AbsEvent * anEvent)
{
  PrintReport();

  return true;
}

int RACStdInputModule::GetEntry(int i)
{
  int nb = fChain->GetEntry(i);
  fNBytes += nb;

  int runNumber = (fEventInfo != NULL) ? fEventInfo->GetRunNumber()     : 0;
  int evtNumber = (fEventInfo != NULL) ? fEventInfo->GetEventNumber()   : i;
  int trgNumber = (fEventInfo != NULL) ? fEventInfo->GetTriggerNumber() : i;

  AbsEvent::Instance()->SetRunNumber(runNumber);
  AbsEvent::Instance()->SetEventNumber(evtNumber);
  AbsEvent::Instance()->SetTriggerNumber(trgNumber);
  AbsEvent::Instance()->SetEntryNumber(fTotalProcessedEntry);


  if(i != 0 && i % fNReport == 0)
    Info("GetEntry", "RUN=%6d  TRG=%8d  ENT=%8lld", 
	 runNumber, trgNumber, fTotalProcessedEntry);

  fTotalProcessedEntry += 1;

  return nb;
}

/**
$Log: RACStdInputModule.cc,v $
Revision 1.16  2016/02/02 09:42:00  jslee
*** empty log message ***

Revision 1.15  2015/10/16 07:46:59  jslee
*** empty log message ***

Revision 1.14  2015/10/08 11:37:32  jslee
*** empty log message ***

Revision 1.13  2015/08/10 12:51:03  jslee
*** empty log message ***

Revision 1.12  2015/08/07 07:55:09  jslee
*** empty log message ***

Revision 1.11  2015/08/07 04:51:10  jslee
*** empty log message ***

Revision 1.10  2015/07/25 10:43:37  daq
update

Revision 1.9  2015/06/06 07:23:06  jslee
*** empty log message ***

Revision 1.8  2015/06/06 06:15:12  jslee
rearrange run & event numbering

Revision 1.7  2015/04/26 06:15:41  jslee
modify AbsEventInfo

Revision 1.6  2015/03/23 03:58:02  jslee
bug fixed, read tree size

**/

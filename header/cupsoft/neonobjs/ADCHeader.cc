//
// Created by cupsoft on 7/1/19.
//
#include <iostream>
#include "TString.h"

#include "ADCHeader.hh"

ClassImp(ADCHeader)

ADCHeader::ADCHeader()
  :TObject()
{
  runnum = 0;
  mid = 0;
  cid = 0;
  dlen = 0; // data length
  for (int i = 0; i < 32; i++) {
    ped[i] = 0; // pedestal
    zero[i] = false;
  }
  ttype = 0; // trigger type
  tdest = 0; // trigger destination
  tnum = 0; // tirgger number
  ttime = 0; // trigger time
  ctnum = 0; // local trigger number
  cttime = 0; // local trigger time
  ctptn = 0; // local tirgger pattern
  error = false;
}

ADCHeader::ADCHeader(const ADCHeader & header)
  :TObject()
{
  runnum = header.runnum;
  mid = header.mid;
  cid = header.cid;
  dlen = header.dlen; // data length
  for (int i = 0; i < 32; i++) {
    ped[i] = header.ped[i]; // pedestal
    zero[i] = header.zero[i];
  }
  ttype = header.ttype; // trigger type
  tdest = header.tdest; // trigger destination
  tnum = header.tnum; // tirgger number
  ttime = header.ttime; // trigger time
  ctnum = header.ctnum; // local trigger number
  cttime = header.cttime; // local trigger time
  ctptn = header.ctptn; // local tirgger pattern
  error = header.error;
}

ADCHeader::~ADCHeader()
{
}


void ADCHeader::Dump() const
{
  std::cout << Form("=============== mid=%d cid=%d ===============", mid, cid) << std::endl;
  std::cout << Form("  dlength=%d, ttype=%d, tdest=%d", dlen, ttype, tdest) << std::endl;
  std::cout << Form("  tnum =%8d, ttime =%ld", tnum, ttime) << std::endl;
  std::cout << Form("  ctnum=%8d, cttime=%ld", ctnum, cttime) << std::endl;
}

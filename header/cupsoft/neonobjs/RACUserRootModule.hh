//--------------------------------------------------------------------------
// File :
//      RACUserRootModule.hh
//
// Description:
//      Class RACUserRootModule is the class for managing user's root file
//      ex. histograms, ntuples ...
//
// Author List:
//      Jaison Lee                Original Author
//
// Jan-30-2007 (Jaison Lee) : RACUserRootModule start
// Nov-25-2013 (Jaison Lee) : modified for general framework
//
//------------------------------------------------------------------------
#ifndef RACUserRootModule_HH
#define RACUserRootModule_HH

//------------------------------------------------------------------------
// C and C++ header
//------------------------------------------------------------------------
#include <string>

//------------------------------------------------------------------------
// Utilities header
//------------------------------------------------------------------------
#include "TString.h"

//------------------------------------------------------------------------
// Collaboration header
//------------------------------------------------------------------------
#include "RACRootModule.hh"
#include "RACRootManager.hh"

class RACUserRootModule : public RACRootModule {
public:
  RACUserRootModule(const char * name  = "RACUserRootModule",
		    const char * title = "Base Class for User Root File Module");
  virtual ~RACUserRootModule();

  virtual bool BeginJob (AbsEvent * anEvent);
  virtual bool EndJob   (AbsEvent * anEvent);
  virtual bool BeginRun (AbsEvent * anEvent){ return true; };
  virtual bool EndRun   (AbsEvent * anEvent){ return true; };
  virtual bool Event    (AbsEvent * anEvent){ return true; };

  void SetUserRootFileName(const char * name);

protected:
  TString          fRootFileName;

  ClassDef(RACUserRootModule, 0)
};

inline void RACUserRootModule::SetUserRootFileName(const char * name) 
{
  fRootFileName = name; 
}

#endif

#ifndef AbsRunInfo_hh
#define AbsRunInfo_hh

#include "TNamed.h"

class AbsRunInfo : public TNamed {
public:
  AbsRunInfo();
  AbsRunInfo(const AbsRunInfo & run);
  virtual ~AbsRunInfo();

  void SetRunNumber(int val);
  int  GetRunNumber() const;

protected:
  int fRunNumber;

  ClassDef(AbsRunInfo, 1)
};

inline void AbsRunInfo::SetRunNumber(int val)     
{
  fRunNumber = val; 
}

inline int AbsRunInfo::GetRunNumber() const 
{
  return fRunNumber; 
}

#endif

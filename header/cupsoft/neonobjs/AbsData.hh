//--------------------------------------------------------------------------
// File :
//      AbsData.hh
//
// Description:
//      Class AbsData is the abstract class of all framework standard data
//
// Author List:
//      Jaison Lee                Original Author
//
// Jan-30-2007 (Jaison Lee) : AbsData start
// Nov-25-2013 (Jaison Lee) : modified for general framework
//
//------------------------------------------------------------------------


#ifndef AbsData_hh
#define AbsData_hh

#include "TNamed.h"

class AbsData : public TNamed {
public:
  AbsData();
  AbsData(const char * name, const char * prodname = "");
  AbsData(const AbsData & data);
  virtual ~AbsData();

  void SetProdName(const char * name);
  const char * GetProdName() const;

  virtual void Dump() const = 0;

  ClassDef(AbsData, 1)
};

inline void AbsData::SetProdName(const char * name) 
{ 
  SetTitle(name);
}

inline const char * AbsData::GetProdName() const 
{
  return GetTitle();
}

#endif

//------------------------------------------------------------------------
// C and C++ header
//------------------------------------------------------------------------
#include <iostream>
#include <iomanip>

//------------------------------------------------------------------------
// Utilities header
//------------------------------------------------------------------------
#include "TString.h"

//------------------------------------------------------------------------
// Collaboration header
//------------------------------------------------------------------------
#include "AbsEvent.hh"
#include "RACFramework.hh"
#include "RACModule.hh"

using namespace std;

ClassImp(RACModule)

RACModule::RACModule(const char * name, const char * title)
:TNamed(name, title),
  fIsEnable(true),
  fIsPassed(false),
  fType(RACModule::USER),
  fProdName(""),
  fFramework()
{
  fCPUTime  = 0;
  fRealTime = 0;
}

RACModule::~RACModule()
{
}

void RACModule::Report() const
{
  TString type;
  if(fType == RACModule::INPUT)
    type = "input";
  else if(fType == RACModule::OUTPUT)
    type = "output";
  else if(fType == RACModule::MANAGER)
    type = "manager";
  else if(fType == RACModule::USER)
    type = "user";
  else
    type = "undefined";

  TString enabled = IsEnabled() ? "yes" : "no";

  cout<<setw(12)<<type<<setw(24)<<GetName()<<setw(12)<<enabled<<endl;
}

void RACModule::ReportTime(int nevent) const
{
  if(!IsEnabled()) return;

  cout<<setw(20)<<GetName()
      <<setw(10)<<Form("%10.1f", fCPUTime)
      <<setw(10)<<Form("%10.1f", fRealTime)
      <<setw(14)<<Form("%10.0f", 1000000*fCPUTime/nevent)
      <<setw(14)<<Form("%10.0f", 1000000*fRealTime/nevent)
      <<endl;
}

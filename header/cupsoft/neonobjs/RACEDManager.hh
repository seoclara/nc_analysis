//--------------------------------------------------------------------------
// File :
//      RACEDManager.hh
//
// Description:
//      Class RACEDManger is the class for Event Display
//
// Author List:
//      Jaison Lee                Original Author
//
// Jan-30-2007 (Jaison Lee) : RACEDManager start
//
// Last Update:      $Author: jslee $
// Update Date:      $Date: 2015/03/19 04:53:06 $
// CVS/RCS Revision: $Revision: 1.4 $
// Status:           $State: Exp $
//--------------------------------------------------------------------------

#ifndef RACEDManger_hh
#define RACEDManger_hh

#include "TNamed.h"
#include "TList.h"

#include "RACAbsDisplay.hh"

class AbsEvent;
class RACFramework;
class RACEDManager : public TNamed {
private:
  RACEDManager();
  virtual ~RACEDManager();

public:
  static RACEDManager * Instance();
  static void DeleteInstance();

  void SetFramework(RACFramework * framework);

  void AddDisplay(RACAbsDisplay * display);
  void ShowDisplay(const char * name);
  void ShowDisplayAll();

private:
  static RACEDManager * sfManager;

  RACFramework * fFramework;

  TList * fDisplays;

  ClassDef(RACEDManager,0)
};

inline void RACEDManager::SetFramework(RACFramework * framework)
{
  fFramework = framework;
}

inline void RACEDManager::AddDisplay(RACAbsDisplay * display)
{
  fDisplays->Add(display);
}

#endif

/**
$Log: RACEDManager.hh,v $
Revision 1.4  2015/03/19 04:53:06  jslee
delete static member for signleton

**/

//--------------------------------------------------------------------------
// File :
//      AbsEvent.cc
//
// Description:
//      Class AbsEvent is the main class for an event for framework
//
// Author List:
//      Jaison Lee                Original Author
//
// Jan-30-2007 (Jaison Lee) : AbsEvent start
// Nov-25-2013 (Jaison Lee) : modified for general framework
//
// Last Update:      $Author: cupsoft $
// Update Date:      $Date: 2018/11/20 07:54:48 $
// CVS/RCS Revision: $Revision: 1.14 $
// Status:           $State: Exp $
//------------------------------------------------------------------------

#include <iostream>
#include <iomanip>

#include "TROOT.h"
#include "TBranch.h"
#include "TClass.h"
#include "TObjString.h"

#include "AbsEvent.hh"
#include "AbsNode.hh"
#include "AbsData.hh"

using namespace std;

ClassImp(AbsEvent)

AbsEvent * AbsEvent::sfTheEvent = NULL;

AbsEvent::AbsEvent()
{
  fListOfNodes = new TObjArray(10);
  fListOfInputNodes = new TObjArray(10);
  fListOfOutputNodes = new TObjArray(10);
  fDropInputList = new TObjArray(10);
  fDropOutputList = new TObjArray(10);
  fListOfObject = new TObjArray(10);

  fRunInfo = NULL;
  fEventInfo = NULL;
}

AbsEvent::~AbsEvent()
{
  fListOfNodes->Delete();
  //fListOfInputNodes->Delete();
  fDropInputList->Delete();
  fDropOutputList->Delete();
}

AbsEvent * AbsEvent::Instance()
{
  if (sfTheEvent == NULL) sfTheEvent = new AbsEvent();
  return sfTheEvent;
}

void AbsEvent::DeleteInstance()
{
  if (sfTheEvent != NULL) {
    delete sfTheEvent;
  }

  sfTheEvent = NULL;
}

int AbsEvent::AddData(const char * branchName, AbsData * data)
{
  TClass * cl = gROOT->GetClass(branchName);
  if (!cl) {
    Error("AddData", "class %s doesn\'t have a dictionary", branchName);
    return -1;
  }

  if (!cl->InheritsFrom("AbsData")) {
    Error("AddData", "class %s doesn\'t inherit from AbsData", branchName);
    return -1;
  }

  AbsNode * node = new AbsNode(branchName, data, this);
  fListOfNodes->Add(node);

  if (cl->InheritsFrom("AbsEventInfo")) {
    fEventInfo = node;
  }

  //Info("AddData", Form("branch %s::%s is added", node->GetName(), node->GetDataProdName()));
  Info("AddData", "branch %s::%s is added", node->GetName(), node->GetDataProdName());

  return 0;
}

int AbsEvent::AddInputData(TBranch * branch, AbsNode *& node)
{
  const char * branchName = branch->GetName();

  AbsNode * temp = (AbsNode *)fListOfInputNodes->FindObject(branchName);
  if (temp) {
    Info("AddInputData", "branch %s has been added to input node already", branchName);
    node = temp;
    return 0;
  }

  TClass * cl = gROOT->GetClass(branchName);
  if (!cl) {
    Error("AddInputData", "branch %s doesn\'t have a dictionary", branchName);
    node = NULL;
    return -1;
  }

  if (!cl->InheritsFrom("AbsData")) {
    Error("AddInputData", "branch %s doesn\'t inherit from AbsData", branchName);
    node = NULL;
    return 1;
  }

  TObjString * name = (TObjString *)fDropInputList->FindObject(branchName);
  if (name) {
    Info("AddInputData", "branch %s is supposed to be dropped from input list", branchName);
    node = NULL;
    return 0;
  }

  node = new AbsNode(branchName, cl, this);
  node->SetSplitLevel(branch->GetSplitLevel());
  fListOfInputNodes->Add(node);

  if (cl->InheritsFrom("AbsEventInfo")) {
    fEventInfo = node;
  }

  Info("AddInputData", "branch %s is added to input node", branchName);

  return 0;
}

int AbsEvent::AddDropInputData(const char * branchName, const char * prodName)
{
  TNamed * name = new TNamed(branchName, prodName);
  fDropInputList->Add(name);

  return 0;
}

int AbsEvent::AddDropOutputData(const char * branchName, const char * prodName)
{
  TNamed * name = new TNamed(branchName, prodName);
  fDropOutputList->Add(name);

  return 0;
}

TObjArray * AbsEvent::GetOutputData()
{
  AbsNode * node;

  bool dropIt;

  TIter in(fListOfInputNodes);
  while ((node = (AbsNode *)in.Next())) {
    dropIt = false;
    TNamed * name = (TNamed *)fDropOutputList->FindObject(node->GetName());
    if (name) {
      if (TString(name->GetTitle()) == "") {
        dropIt = true;
      }
      else if (TString(name->GetTitle()) == node->GetDataProdName()) {
        dropIt = true;
      }
    }

    if (dropIt) {
      Info("GetOutputData",
           "node %s::%s is supposed to be dropped from output",
           node->GetName(),
           node->GetDataProdName());
      continue;
    }

    fListOfOutputNodes->Add(node);
  }

  TIter da(fListOfNodes);
  while ((node = (AbsNode *)da.Next())) {
    dropIt = false;
    TNamed * name = (TNamed *)fDropOutputList->FindObject(node->GetName());
    if (name) {
      if (TString(name->GetTitle()) == "") {
        dropIt = true;
      }
      else if (TString(name->GetTitle()) == node->GetDataProdName()) {
        dropIt = true;
      }
    }

    if (dropIt) {
      Info("GetOutputData",
           "node %s::%s is supposed to be dropped from output",
           node->GetName(),
           node->GetDataProdName());
      continue;
    }

    fListOfOutputNodes->Add(node);
  }

  TIter iter(fListOfOutputNodes);
  while ((node = (AbsNode *)iter.Next())) {
    Info("GetOutputData",
         "node %s::%s is added to output",
         node->GetName(),
         node->GetDataProdName());
  }

  return fListOfOutputNodes;
}

AbsData * AbsEvent::GetData(const char * name, const char * prodName)
{
  AbsNode * node = NULL;

  int nobj = fListOfInputNodes->GetEntries();

  for (int i = 0; i < nobj; i++) {
    AbsNode * temp = (AbsNode *)fListOfInputNodes->At(i);
    AbsData * data = temp->GetData();

    TString tempname1 = temp->GetName();
    TString tempname2 = data->GetProdName();

    if (prodName == NULL) {
      if (tempname1.EqualTo(name)) {
        node = temp;
      }
    }
    else {
      if (tempname1.EqualTo(name) && tempname2.EqualTo(prodName)) {
        node = temp;
      }
    }
  }

  if (!node) {
    nobj = fListOfNodes->GetEntries();
    for (int i = 0; i < nobj; i++) {
      AbsNode * temp = (AbsNode *)fListOfNodes->At(i);
      AbsData * data = temp->GetData();

      TString tempname1 = temp->GetName();
      TString tempname2 = data->GetProdName();

      if (prodName == NULL) {
        if (tempname1.EqualTo(name)) {
          node = temp;
        }
      }
      else {
        if (tempname1.EqualTo(name) && tempname2.EqualTo(prodName)) {
          node = temp;
        }
      }
    }

    if (!node) {
      Error("GetData", "there is no data named %s::%s ", name, prodName);
      return NULL;
    }
  }

  return node->GetData();
}

AbsEventInfo * AbsEvent::GetEventInfo() const
{
  if (fEventInfo == NULL) {
    Error("GetEventInfo", "Event information data is missing ....");
    return NULL;
  }

  return (AbsEventInfo *)fEventInfo->GetData();
}

/**
$Log: AbsEvent.cc,v $
Revision 1.14  2018/11/20 07:54:48  cupsoft
*** empty log message ***

Revision 1.13  2015/08/07 07:55:09  jslee
*** empty log message ***

Revision 1.12  2015/08/07 04:51:10  jslee
*** empty log message ***

Revision 1.11  2015/07/25 10:43:37  daq
update

Revision 1.10  2015/06/06 07:23:06  jslee
*** empty log message ***

Revision 1.9  2015/03/19 04:53:06  jslee
delete static member for signleton

**/

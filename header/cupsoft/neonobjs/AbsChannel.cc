/*
 *
 *  Module:  AbsChannel/RawObjs
 *
 *  Author:  Jaison Lee
 *
 *  Purpose: A Class for storing basic channel information
 *
 *  Last Update:      $Author: cupsoft $
 *  Update Date:      $Date: 2016/07/14 07:58:51 $
 *  CVS/RCS Revision: $Revision: 1.1.1.1 $
 *  Status:           $State: Exp $
 *
 */

#include "AbsChannel.hh"

ClassImp(AbsChannel)

AbsChannel::AbsChannel()
: TObject()
{
  fID  = 0;
  fMID = 0;
  fCID = 0;
  fBit = 0;
}

AbsChannel::AbsChannel(Int_t id)
  : TObject()
{
  fID  = id;
  fMID = 0;
  fCID = 0;
  fBit = 0;
}

AbsChannel::AbsChannel(const AbsChannel & ch)
  : TObject(ch)
{
  fID  = ch.GetID();
  fMID = ch.GetMID();
  fCID = ch.GetCID();
  fBit = ch.GetBit();
}

AbsChannel::~AbsChannel()
{
}

/**
$Log: AbsChannel.cc,v $
Revision 1.1.1.1  2016/07/14 07:58:51  cupsoft
RawObjs

Revision 1.1.1.1  2016/02/29 08:25:13  cupsoft
RawObjs

**/

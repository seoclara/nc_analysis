//--------------------------------------------------------------------------
// File :
//      RACEDManager.cc
//
// Description:
//      Class RACEDManger is the class for Event Display
//
// Author List:
//      Jaison Lee                Original Author
//
// Jan-30-2007 (Jaison Lee) : RACEDManager start
//
// Last Update:      $Author: jslee $
// Update Date:      $Date: 2015/03/19 04:53:06 $
// CVS/RCS Revision: $Revision: 1.3 $
// Status:           $State: Exp $
//--------------------------------------------------------------------------


#include "AbsEvent.hh"
#include "RACFramework.hh"
#include "RACEDManager.hh"

ClassImp(RACEDManager)

RACEDManager * RACEDManager::sfManager = NULL;

RACEDManager::RACEDManager()
  :TNamed()
{						
  fFramework = NULL;
  fDisplays  = new TList();
}

RACEDManager::~RACEDManager()
{
  fDisplays->Delete();
  delete fDisplays;
}

RACEDManager * RACEDManager::Instance()
{
  if(sfManager == 0) sfManager = new RACEDManager();
  return sfManager;
}

void RACEDManager::DeleteInstance()
{
  if(sfManager != NULL)
    delete sfManager;

  sfManager = NULL;
}


void RACEDManager::ShowDisplay(const char * name)
{
  TIter it(fDisplays);
  while(RACAbsDisplay * display = (RACAbsDisplay*)it.Next()){
    if(TString(display->GetName()).Contains(name))
      display->ShowDisplay();
  }
} 

void RACEDManager::ShowDisplayAll()
{
  TIter it(fDisplays);
  while(RACAbsDisplay * display = (RACAbsDisplay*)it.Next()){
    display->ShowDisplay();
  }
} 

/**
$Log: RACEDManager.cc,v $
Revision 1.3  2015/03/19 04:53:06  jslee
delete static member for signleton

**/

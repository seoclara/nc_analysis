#include "TBranch.h"
#include "TClass.h"

#include "AbsNode.hh"
#include "AbsData.hh"

ClassImp(AbsNode)

AbsNode::AbsNode()
  :TNamed()
{
  fObject       = 0;
  fDeleteObject = 0;

  fSplitLevel = 99;
}

AbsNode::AbsNode(const char * branchName, TClass * cl, AbsEvent * event)
  :TNamed(branchName, branchName)
{
  fBranch = NULL;
  fEvent  = event;
  fObject = (AbsData*)cl->New();
  fDeleteObject = 1;

  fSplitLevel = 99;
}

AbsNode::AbsNode(TBranch * branch, TClass * cl, AbsEvent * event)
  :TNamed(branch->GetName(), branch->GetName())
{
  fBranch = branch;
  fEvent  = event;
  fObject = (AbsData*)cl->New();
  fDeleteObject = 1;

  fSplitLevel = 99;
}

AbsNode::AbsNode(const char * branchName, AbsData * data, AbsEvent * event)
  :TNamed(branchName, branchName)
{
  fBranch = NULL;
  fEvent  = event;
  fObject = data;
  fDeleteObject = 0;

  fSplitLevel = 99;
}

AbsNode::~AbsNode()
{
  if(fDeleteObject)
    delete fObject;
}

int AbsNode::GetEntry(int ientry)
{
  if(fBranch)
    return fBranch->GetEntry(ientry);
  else
    return -1;
}

//--------------------------------------------------------------------------
// File :
//      RACDBManager.cc
//
// Description:
//      Class RACDBManger is the class for Database access
//
// Author List:
//      Jaison Lee                Original Author
//
// Jan-30-2007 (Jaison Lee) : RACDBManager start
//
// Last Update:      $Author: jslee $
// Update Date:      $Date: 2015/03/19 04:53:06 $
// CVS/RCS Revision: $Revision: 1.2 $
// Status:           $State: Exp $
//--------------------------------------------------------------------------

#include <fstream>
#include <sstream>

#include "RACDBManager.hh"

using namespace std;

ClassImp(RACDBManager)

RACDBManager * RACDBManager::sfManager = NULL;

RACDBManager::RACDBManager(const char * name, const char * title)
  :RACModule(name, title)
{
}

RACDBManager::~RACDBManager()
{
}

RACDBManager * RACDBManager::Instance()
{
  if(sfManager == NULL) sfManager = new RACDBManager();
  return sfManager;
}

void RACDBManager::DeleteInstance()
{
  if(sfManager != NULL)
    delete sfManager;

  sfManager = NULL;
}

bool RACDBManager::BeginJob(AbsEvent * anEvent)
{
  return true;
}

bool RACDBManager::EndJob(AbsEvent * anEvent)
{
  return true;
}

bool RACDBManager::BeginRun(AbsEvent * anEvent)
{
  return true;
}

bool RACDBManager::EndRun(AbsEvent * anEvent)
{
  return true;
}

/**
$Log: RACDBManager.cc,v $
Revision 1.2  2015/03/19 04:53:06  jslee
delete static member for signleton

**/

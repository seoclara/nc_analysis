/*
 *
 *  Module:  AChannelData/RawObjs
 *
 *  Author:  Jaison Lee
 *
 *  Purpose: TClonesArray for AChannels
 *
 *  Last Update:      $Author: cupsoft $
 *  Update Date:      $Date: 2016/07/14 07:58:51 $
 *  CVS/RCS Revision: $Revision: 1.1.1.1 $
 *  Status:           $State: Exp $
 *
 */

#ifndef AChannelData_hh
#define AChannelData_hh

#include "TClonesArray.h"

#include "AbsData.hh"

typedef TClonesArray AChannelColl;

class AChannel;
class AChannelData : public AbsData 
{
public:
  AChannelData();
  AChannelData(const AChannelData & data);
  virtual ~AChannelData();
  
  virtual void Clear(const Option_t * opt = "");
  virtual void Dump() const;
  
  AChannel * Add();
  AChannel * Add(Int_t id);
  AChannel * Add(AChannel * ch);

  AChannelColl * GetColl()    const;
  Int_t          GetN()       const;
  AChannel     * Get(Int_t n) const;

  AChannel     * GetChannel(Int_t id) const;

  void CopyFrom(const AChannelData * data);

private:
  Int_t          fNCh ;//! just for counter
  AChannelColl * fColl;
  
  ClassDef(AChannelData, 1)
};

//
// Inline functions
//

inline AChannelColl * AChannelData::GetColl() const
{
  return fColl; 
}

inline Int_t AChannelData::GetN() const
{
  return fColl->GetEntries(); 
}

inline AChannel * AChannelData::Get(Int_t n) const
{
  return (AChannel*)fColl->At(n); 
}

#endif

/**
$Log: AChannelData.hh,v $
Revision 1.1.1.1  2016/07/14 07:58:51  cupsoft
RawObjs

Revision 1.1.1.1  2016/02/29 08:25:13  cupsoft
RawObjs

**/

//------------------------------------------------------------------------
// C and C++ header
//------------------------------------------------------------------------

//------------------------------------------------------------------------
// Utilities header
//------------------------------------------------------------------------

//------------------------------------------------------------------------
// Collaboration header
//------------------------------------------------------------------------
#include "RACRootModule.hh"
#include "RACRootManager.hh"

ClassImp(RACRootModule)

RACRootModule::RACRootModule(const char * name, const char * title)
: RACModule(name, title)
{
  fRootManager = RACRootManager::Instance();
}

RACRootModule::~RACRootModule()
{
  fRootManager->DeleteInstance();
}



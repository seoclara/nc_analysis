// @(#)root/cont:$Id: ArrayI.cc,v 1.1 2020/01/14 02:26:08 cupsoft Exp $
// Author: Rene Brun   06/03/95

/*************************************************************************
 * Copyright (C) 1995-2000, Rene Brun and Fons Rademakers.               *
 * All rights reserved.                                                  *
 *                                                                       *
 * For the licensing terms see $ROOTSYS/LICENSE.                         *
 * For the list of contributors see $ROOTSYS/README/CREDITS.             *
 *************************************************************************/

/** \class ArrayI
\ingroup Containers
Array of integers (32 bits per element).
*/

#include "ArrayI.hh"
#include "TBuffer.h"

ClassImp(ArrayI);

////////////////////////////////////////////////////////////////////////////////
/// Default ArrayI ctor.

ArrayI::ArrayI()
{
  fArray = 0;
}

////////////////////////////////////////////////////////////////////////////////
/// Create ArrayI object and set array size to n integers.

ArrayI::ArrayI(Int_t n)
{
  fArray = 0;
  if (n > 0) Set(n);
}

////////////////////////////////////////////////////////////////////////////////
/// Create ArrayI object and initialize it with values of array.

ArrayI::ArrayI(Int_t n, const unsigned int * array)
{
  fArray = 0;
  Set(n, array);
}

////////////////////////////////////////////////////////////////////////////////
/// Copy constructor.

ArrayI::ArrayI(const ArrayI & array)
  : TArray(array)
{
  fArray = 0;
  Set(array.fN, array.fArray);
}

////////////////////////////////////////////////////////////////////////////////
/// ArrayI assignment operator.

ArrayI & ArrayI::operator=(const ArrayI & rhs)
{
  if (this != &rhs) {
     Set(rhs.fN, rhs.fArray);
  }
  return *this;
}

////////////////////////////////////////////////////////////////////////////////
/// Delete ArrayI object.

ArrayI::~ArrayI()
{
  delete[] fArray;
  fArray = 0;
}

////////////////////////////////////////////////////////////////////////////////
/// Adopt array arr into ArrayI, i.e. don't copy arr but use it directly
/// in ArrayI. User may not delete arr, ArrayI dtor will do it.

void ArrayI::Adopt(Int_t n, unsigned int * arr)
{
  if (fArray) {
     delete[] fArray;
  }

  fN = n;
  fArray = arr;
}

////////////////////////////////////////////////////////////////////////////////
/// Add Int_t c at position i. Check for out of bounds.

void ArrayI::AddAt(unsigned int c, Int_t i)
{
  if (!BoundsOk("ArrayI::AddAt", i)) return;
  fArray[i] = c;
}

////////////////////////////////////////////////////////////////////////////////
/// Set size of this array to n ints.
/// A new array is created, the old contents copied to the new array,
/// then the old array is deleted.
/// This function should not be called if the array was declared via Adopt.

void ArrayI::Set(Int_t n)
{
  if (n < 0) return;
  if (n != fN) {
    unsigned int * temp = fArray;
    if (n != 0) {
      fArray = new unsigned int[n];
      if (n < fN) { memcpy(fArray, temp, n*sizeof(unsigned int)); }
      else {
        memcpy(fArray, temp, fN*sizeof(unsigned int));
        memset(&fArray[fN], 0, (n - fN)*sizeof(unsigned int));
      }
    }
    else {
      fArray = 0;
    }
    if (fN) delete[] temp;
    fN = n;
  }
}

////////////////////////////////////////////////////////////////////////////////
/// Set size of this array to n ints and set the contents.
/// This function should not be called if the array was declared via Adopt.

void ArrayI::Set(Int_t n, const unsigned int * array)
{
  if (fArray && fN != n) {
    delete[] fArray;
    fArray = 0;
  }
  fN = n;
  if (fN == 0) return;
  if (array == 0) return;
  if (!fArray) fArray = new unsigned int[fN];
  memmove(fArray, array, n*sizeof(unsigned int));
}

////////////////////////////////////////////////////////////////////////////////
/// Stream a ArrayI object.

// void ArrayI::Streamer(TBuffer & b)
// {
//   if (b.IsReading()) {
//     Int_t n;
//     b >> n;
//     Set(n);
//     b.ReadFastArray(fArray, n);
//   }
//   else {
//     b << fN;
//     b.WriteFastArray(fArray, fN);
//   }
// }


/*
 *
 *  Module:  AChannel/RawObjs
 *
 *  Author:  Jaison Lee
 *
 *  Purpose: A Class for storing ADC information
 *
 *  Last Update:      $Author: cupsoft $
 *  Update Date:      $Date: 2016/07/14 07:58:51 $
 *  CVS/RCS Revision: $Revision: 1.1.1.1 $
 *  Status:           $State: Exp $
 *
 */

#include "AChannel.hh"

ClassImp(AChannel)

AChannel::AChannel()
: AbsChannel()
{
  fCount = 0;
  fTime  = 0;
}

AChannel::AChannel(Int_t id)
  : AbsChannel(id)
{
  fCount = 0;
  fTime  = 0;
}

AChannel::AChannel(const AChannel & ch)
  : AbsChannel(ch)
{
  fCount = ch.GetCount();
  fTime  = ch.GetTime();
}

AChannel::~AChannel()
{
}

/**
$Log: AChannel.cc,v $
Revision 1.1.1.1  2016/07/14 07:58:51  cupsoft
RawObjs

Revision 1.1.1.1  2016/02/29 08:25:13  cupsoft
RawObjs

**/

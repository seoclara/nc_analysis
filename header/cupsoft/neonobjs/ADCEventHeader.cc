//
// Created by cupsoft on 7/1/19.
//
#include <iostream>
#include "TString.h"

#include "ADCEventHeader.hh"

ClassImp(ADCEventHeader)

ADCEventHeader::ADCEventHeader()
  :TObject()
{
  mid = 0;
  ctnum = 0; // local trigger number
  cttime = 0; // local trigger time
}

ADCEventHeader::ADCEventHeader(const ADCHeader & header)
  :TObject()
{
  mid = header.mid;
  ctnum = header.ctnum; // local trigger number
  cttime = header.cttime; // local trigger time
}

ADCEventHeader::ADCEventHeader(const ADCEventHeader & header)
  :TObject()
{
  mid = header.GetMID();
  ctnum = header.GetTriggerNumber(); // local trigger number
  cttime = header.GetTriggerTime(); // local trigger time
}

ADCEventHeader::~ADCEventHeader()
{
}


void ADCEventHeader::Dump() const
{
  std::cout << Form("=============== mid=%d ===============", mid) << std::endl;
  std::cout << Form("  ctnum=%8d, cttime=%ld", ctnum, cttime) << std::endl;
}

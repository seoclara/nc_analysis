#ifndef RACRootModule_HH
#define RACRootModule_HH

//------------------------------------------------------------------------
// C and C++ header
//------------------------------------------------------------------------

//------------------------------------------------------------------------
// Utilities header
//------------------------------------------------------------------------
#include "TString.h"

//------------------------------------------------------------------------
// Collaboration header
//------------------------------------------------------------------------
#include "RACModule.hh"

class RACRootManager;
class RACRootModule : public RACModule {
public:
  RACRootModule(const char * name  = "RACRootModule",
		const char * title = "Base Class for Root File Module");
  virtual ~RACRootModule();

  RACRootManager * GetRootManager();

protected:
  RACRootManager * fRootManager;

  ClassDef(RACRootModule, 0)
};

inline RACRootManager * RACRootModule::GetRootManager()
{
  return fRootManager;
}

#endif

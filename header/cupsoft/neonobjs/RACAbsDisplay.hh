#ifndef RACAbsDisplay_hh
#define RACAbsDisplay_hh

#include "TGFrame.h"

#include "AbsEvent.hh"

class RACAbsDisplay : public TGMainFrame {
public:
  RACAbsDisplay(const char * name = "RACAbsDisplay");
  virtual ~RACAbsDisplay();

  virtual void FillEvent(AbsEvent * anEvent) = 0;

  virtual const char * GetName() const;
  virtual void ShowDisplay();


private:
  TString fDisplayName;

  ClassDef(RACAbsDisplay, 0)
};

inline const char * RACAbsDisplay::GetName() const
{
  return fDisplayName;
}

#endif

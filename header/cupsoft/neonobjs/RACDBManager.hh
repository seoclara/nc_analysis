//--------------------------------------------------------------------------
// File :
//      RACDBManager.hh
//
// Description:
//      Class RACDBManger is the class for Database access
//
// Author List:
//      Jaison Lee                Original Author
//
// Jan-30-2007 (Jaison Lee) : RACDBManager start
//
// Last Update:      $Author: jslee $
// Update Date:      $Date: 2015/03/19 04:53:06 $
// CVS/RCS Revision: $Revision: 1.2 $
// Status:           $State: Exp $
//--------------------------------------------------------------------------

#ifndef RACDBManager_HH
#define RACDBManager_HH

#include <iostream>

#include "TString.h"

#include "AbsEvent.hh"
#include "RACModule.hh"

class RACDBManager : public RACModule {
private:
  RACDBManager(const char * name  = "RACDBManager",
	       const char * title = "RACDBManager");
  virtual ~RACDBManager();

public:
  static RACDBManager * Instance();
  static void DeleteInstance();

  virtual bool BeginJob (AbsEvent * anEvent);
  virtual bool EndJob   (AbsEvent * anEvent);
  virtual bool BeginRun (AbsEvent * anEvent);
  virtual bool EndRun   (AbsEvent * anEvent);
  virtual bool Event    (AbsEvent * anEvent){ return true; }

private:
  static RACDBManager * sfManager;

  ClassDef(RACDBManager, 0)    
};

#endif

/**
$Log: RACDBManager.hh,v $
Revision 1.2  2015/03/19 04:53:06  jslee
delete static member for signleton

**/


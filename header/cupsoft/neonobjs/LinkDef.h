//#ifdef __CLING__

#pragma link off all globals;
#pragma link off all classes;
#pragma link off all functions;

#pragma link C++ class AbsData+;
#pragma link C++ class AbsEventInfo+;
#pragma link C++ class AbsEvent+;
#pragma link C++ class AbsNode+;
#pragma link C++ class AbsRunInfo+;
#pragma link C++ class EventInfo+;
#pragma link C++ class AbsChannel+;
#pragma link C++ class AChannel+;
#pragma link C++ class AChannelData+;
#pragma link C++ class FChannelS+;
#pragma link C++ class FChannelSData+;
#pragma link C++ class RACRootManager+;
#pragma link C++ class RACRootModule+;
#pragma link C++ class RACModule+;
#pragma link C++ class RACFramework+;
#pragma link C++ class RACStdInputModule+;
#pragma link C++ class RACDBManager+;
#pragma link C++ class RACAbsDisplay+;
#pragma link C++ class RACEDManager+;
#pragma link C++ class RACStdOutputModule+;
#pragma link C++ class RACInputModule+;
#pragma link C++ class RACUserRootModule+;
#pragma link C++ class ADCEventHeader+;
#pragma link C++ class ADCHeader+;
#pragma link C++ class FChannelI+;
#pragma link C++ class FChannelIData+;
#pragma link C++ class ArrayS+;
#pragma link C++ class ArrayI+;
#pragma link C++ class ADCEventHeader+;

//#endif

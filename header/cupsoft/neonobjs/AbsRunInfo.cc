#include "AbsRunInfo.hh"

ClassImp(AbsRunInfo)

AbsRunInfo::AbsRunInfo()
:TNamed("AbsRunInfo", "AbsRunInfo")
{
  fRunNumber = -999;
}

AbsRunInfo::AbsRunInfo(const AbsRunInfo & run)
  :TNamed(run)
{
  fRunNumber = run.GetRunNumber();
}

AbsRunInfo::~AbsRunInfo()
{
}

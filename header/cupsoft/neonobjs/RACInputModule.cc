//--------------------------------------------------------------------------
// File :
//      RACInputModule.cc
//
// Description:
//      Class RACInputModule is the abstract class for managubg root input
//
// Author List:
//      Jaison Lee                Original Author
//
// Jan-30-2007 (Jaison Lee) : RACInputModule start
// Nov-25-2013 (Jaison Lee) : modified for general framework
//
// Last Update:      $Author: jslee $
// Update Date:      $Date: 2015/03/23 03:58:02 $
// CVS/RCS Revision: $Revision: 1.7 $
// Status:           $State: Exp $
//------------------------------------------------------------------------

#include <iostream>
#include <iomanip>

#include "TChainElement.h"

#include "RACFramework.hh"
#include "RACInputModule.hh"

using namespace std;

ClassImp(RACInputModule)

RACInputModule::RACInputModule(const char * name, const char * title)
  :RACModule(name, title)
{
  SetType(RACModule::INPUT);
  fOwnChain = false;
  fChain    = NULL ;

  fNBytes = 0;

  fNReport = 100;
  fTotalProcessedEntry = 0;
}

RACInputModule::~RACInputModule()
{
  if(fOwnChain)
    delete fChain;
}

void RACInputModule::InitChain(const char * treeName)
{
  fOwnChain = true;
  fChain = new TChain(treeName);
}

int RACInputModule::AddFile(const char * fileName)
{
  return fChain->Add(fileName);
}

int RACInputModule::GetEntries()
{
  return fChain->GetEntries();
}


void RACInputModule::PrintReport()
{
  TObjArray * fileList = fChain->GetListOfFiles();

  cout<<endl;
  cout<<"******************************************************************"<<endl;
  cout<<"   Input Module Report                                            "<<endl;
  cout<<"   ====================                                           "<<endl;
  cout<<endl;
  cout<<"   Input Files :"<<endl;
  TIter it(fileList);
  while(TChainElement * el = (TChainElement*)it.Next()){
    const char * fname = el->GetTitle();
    cout<<"      "<<fname<<endl;
  }
  cout<<"   Total read [MBytes]     : "<<fNBytes/1000000.0<<endl;
  cout<<"   Total processed Entries : "<<fTotalProcessedEntry<<endl;
  cout<<"******************************************************************"<<endl;
  cout<<endl;
}

/**
$Log: RACInputModule.cc,v $
Revision 1.7  2015/03/23 03:58:02  jslee
bug fixed, read tree size

**/
